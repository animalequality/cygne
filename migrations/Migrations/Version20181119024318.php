<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181119024318 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE petition ADD mailinglist_id VARCHAR(15) DEFAULT NULL COMMENT \'The numeric mailinglist/Mailchimp list id\' AFTER email_subject');
        $default_listid = $this->container->getParameter('default_mailinglist_id');
        if ($default_listid) {
            $this->addSql('UPDATE petition SET mailinglist_id = :id', ['id' => $default_listid]);
        }
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE petition DROP mailinglist_id');
    }
}
