<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Add indexes missing {signature} `confirm_email_id`.
 */
final class Version20210430173733 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add INDEX to {signature}.confirm_email_id';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->skipIf($schema->getTable('signature')->hasIndex('confirm_email_id_idx'), 'confirm_email_id_idx INDEX already exists');
        $this->addSql('CREATE INDEX confirm_email_id_idx ON signature (confirm_email_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->skipIf(! $schema->getTable('signature')->hasIndex('confirm_email_id_idx'), 'confirm_email_id_idx INDEX already exists');
        $this->addSql('DROP INDEX confirm_email_id_idx ON signature');
    }
}
