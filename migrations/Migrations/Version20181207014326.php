<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use App\Entity\Petition;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181207014326 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE petition ADD requirements SMALLINT UNSIGNED NOT NULL COMMENT \'Represents which signature fields this petition requires\'');
        $this->addSql('UPDATE petition SET requirements = :level', ['level' => Petition::REQUIRE_ALL]);
        $this->addSql('ALTER TABLE 
          signature
          CHANGE city city VARCHAR(100) DEFAULT NULL COMMENT \'signer\'\'s city\', 
          CHANGE postal_code postal_code VARCHAR(30) DEFAULT NULL COMMENT \'signer\'\'s postal code\', 
          CHANGE country country VARCHAR(2) DEFAULT NULL COMMENT \'signer\'\'s country (ISO code)\'');

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE petition DROP requirements');
        $this->addSql('ALTER TABLE 
          signature
          CHANGE city city VARCHAR(100) NOT NULL COLLATE utf8mb4_unicode_ci, 
          CHANGE postal_code postal_code VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci, 
          CHANGE country country VARCHAR(2) NOT NULL COLLATE utf8mb4_unicode_ci');

    }
}
