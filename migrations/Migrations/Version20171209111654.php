<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171209111654 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE signature ADD petition_id INT DEFAULT NULL, ADD confirm_email_mandrill_id VARCHAR(255) DEFAULT NULL, ADD confirm_email_error VARCHAR(255) DEFAULT NULL, DROP confirmation_email_mandrill_id, DROP confirmation_email_error, CHANGE confirmation_email_status confirm_email_status VARCHAR(20) DEFAULT \'pending\' NOT NULL');
        $this->addSql('ALTER TABLE signature ADD CONSTRAINT FK_AE880141AEC7D346 FOREIGN KEY (petition_id) REFERENCES petition (id)');
        $this->addSql('CREATE INDEX IDX_AE880141AEC7D346 ON signature (petition_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE signature DROP FOREIGN KEY FK_AE880141AEC7D346');
        $this->addSql('DROP INDEX IDX_AE880141AEC7D346 ON signature');
        $this->addSql('ALTER TABLE signature ADD confirmation_email_mandrill_id VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, ADD confirmation_email_error VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, DROP petition_id, DROP confirm_email_mandrill_id, DROP confirm_email_error, CHANGE confirm_email_status confirmation_email_status VARCHAR(20) DEFAULT \'pending\' NOT NULL COLLATE utf8_unicode_ci');
    }
}
