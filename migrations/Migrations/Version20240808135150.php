<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Remove columns which only live in YAML from now on
 */
final class Version20210430173733 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Remove columns which only live in YAML from now on';
    }

    /*
      petitions.yml YAML configuration update:
      $ yq -yiw500 '{"petitions": .petitions|map(.|del(.subtitle, .email_from_name, .email_from_email, .email_reply_to, .email_subject, .extra.email, .share_site_url, .share_twitter_url, .share_facebook_url, .donation_campaign) * {"messages": {"_": {"subtitle": .subtitle, "email_from_name": .email_from_name, "email_from_email": .email_from_email, "email_reply_to": .email_reply_to, "email_subject": .email_subject, "form_page_url": .share_site_url, "share_twitter_url": .share_twitter_url, "share_facebook_url": .share_facebook_url, "donation_campaign": .donation_campaign, "extra": .extra.email}}})}' petitions.yml
    */
    public function up(Schema $schema): void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $table = $schema->getTable('petition');

        $table->dropColumn('subtitle');
        $table->dropColumn('email_from_name');
        $table->dropColumn('email_from_email');
        $table->dropColumn('email_reply_to');
        $table->dropColumn('email_subject');
        $table->dropColumn('donation_campaign');
        $table->dropColumn('share_site_url');
        $table->dropColumn('share_twitter_url');
        $table->dropColumn('share_facebook_url');

    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}
