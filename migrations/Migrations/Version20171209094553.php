<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171209094553 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE signature (id INT AUTO_INCREMENT NOT NULL, created_at DATETIME NOT NULL, revoked_at DATETIME DEFAULT NULL, first_name VARCHAR(100) NOT NULL, last_name VARCHAR(100) NOT NULL, email VARCHAR(100) NOT NULL, address VARCHAR(255) NOT NULL, city VARCHAR(100) NOT NULL, postal_code VARCHAR(30) NOT NULL, country VARCHAR(2) NOT NULL, ip_address VARCHAR(255) NOT NULL, status VARCHAR(20) DEFAULT \'accepted\' NOT NULL, confirmation_email_status VARCHAR(20) DEFAULT \'pending\' NOT NULL, confirmation_email_mandrill_id VARCHAR(255) DEFAULT NULL, confirmation_email_error VARCHAR(255) DEFAULT NULL, newsletter_status VARCHAR(20) DEFAULT \'denied\' NOT NULL, newsletter_error VARCHAR(255) DEFAULT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE signature');
    }
}
