<?php declare(strict_types = 1);

namespace DoctrineMigrations;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171209151430 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE signature CHANGE confirm_email_status confirm_email_status VARCHAR(20) DEFAULT \'todo\' NOT NULL, CHANGE newsletter_status newsletter_status VARCHAR(20) DEFAULT \'todo\' NOT NULL, CHANGE confirm_email_mandrill_id confirm_email_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('CREATE INDEX confirm_email_status_idx ON signature (confirm_email_status)');
        $this->addSql('CREATE INDEX newsletter_status_idx ON signature (newsletter_status)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP INDEX confirm_email_status_idx ON signature');
        $this->addSql('DROP INDEX newsletter_status_idx ON signature');
        $this->addSql('ALTER TABLE signature CHANGE confirm_email_status confirm_email_status VARCHAR(20) DEFAULT \'pending\' NOT NULL COLLATE utf8_unicode_ci, CHANGE newsletter_status newsletter_status VARCHAR(20) DEFAULT \'denied\' NOT NULL COLLATE utf8_unicode_ci, CHANGE confirm_email_id confirm_email_mandrill_id VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
