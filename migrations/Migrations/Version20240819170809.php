<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20240819170809 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE signature ADD updated_at DATETIME DEFAULT NULL COMMENT \'time when the signature was updated\' AFTER created_at');
        $this->addSql('CREATE INDEX updated_at_idx ON signature (updated_at)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $table = $schema->getTable('signature');
        $this->dropColumn('updated_at');
        $this->dropIndex('updated_at_idx');
    }
}
