<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181119034209 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE petition ADD mailchimp_groups VARCHAR(50) DEFAULT NULL COMMENT \'Comma-separated hex words of mailchimp interest groups\'');
        $default_groups = $this->container->getParameter('default_mailchimp_groups') ? : '';
        $default_groups = array_filter(explode(',', $default_groups));
        if ($default_groups) {
            $this->addSql('UPDATE petition SET mailchimp_groups = :groups', ['groups' => implode(',', $default_groups)]);
        }
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE petition DROP mailchimp_groups');
    }
}
