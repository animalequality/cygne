<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20190426213829 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'mailchimp_groups is renamed to groups to reflect the fact that the use of this column has been extended by new synchronization methods.';
    }

    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE petition CHANGE mailchimp_groups groups VARCHAR(255) DEFAULT NULL COMMENT \'Comma-separated list of groups to subscribe to\' AFTER mailchimp_source');
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('ALTER TABLE petition CHANGE groups mailchimp_groups VARCHAR(50) DEFAULT NULL COMMENT \'Comma-separated list of groups to subscribe to\'');
    }
}
