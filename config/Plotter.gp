#!/usr/bin/env gnuplot

# Copyright (C) 2016, 2019 Raphaël . Droz + floss @ gmail DOT com
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# Draw progression and derived of values along the time.
# Usable signature count evolution
# Input file format: 3 columns: <DATE> <HOUR> <VALUE>

if (!exists("filename")) filename="/tmp/graph.log"
if (!exists("w")) w=1280
if (!exists("h")) h=960
if (!exists("prefix")) prefix=""

# precalculation of GPVAL_Y_MAX

# non-solution 1: Stats command not available in timedata mode (after set xdata time)
# stat filename using 3

# non-solution 2: replot (because multiplot = no reset term/output)
# replot

# solution 3: no output at first and store GPVAL_Y_MAX inside variables. ToDo: timecolumn(1) ?

set term png
set output "/dev/null"

set xdata time
set style data lines  

set timefmt "%Y-%m-%dT%H:%M:%S"

# unset xlabel
set xtics scale 1.5 rotate

set autoscale yfixmax
set autoscale xfixmax

# bottom line to 0
set yrange [0:]

# no legend
unset key

# derive function
d(x, y) = ($0 == 0) ? (y1 = y, 1/0) : (y2 = y1, y1 = y, (y1-y2)/2)

plot filename using 1:2 title "count" w lines
time_min = GPVAL_DATA_X_MIN
time_max = GPVAL_DATA_X_MAX
derive_max = GPVAL_DATA_Y_MAX

count_max=0
add(x)=(count_max=count_max+x)
plot filename using 1:(add($2)) title "count" w lines

# Now the real stuff
set term png size w, h
set output sprintf("%s-%dx%d.png", filename, w, h)

set multiplot layout 2,2

# yeah... http://gnuplot.sourceforge.net/docs_4.2/node295.html
set xtics add (time_max time_max)

# normal graph


set format x "%Y-%m-%d %H:%M"
set grid
set ylabel "Signatures"
set title sprintf("%s signcount over time : %s", prefix, system('basename '. filename))
# yeah...
set ytics add (count_max count_max)
a=0
cumulative_sum(x)=(a=a+x,a)
plot filename using 1 : (cumulative_sum($2)) title "count" w lines

# derivation graphs
set ytics auto
set ytics add (derive_max derive_max)
set grid
set ylabel "Signature variation"
set title  sprintf("%s signatures/minute (derive)", prefix)
plot filename using 1:2 title 'Signatures/minute (derive)'


max_activity = system(sprintf("awk '{if ($2 ~ /%d$/) { split($1, a, \"T\"); \"date +%%s -d \" a[1] | getline x; print(x+int(a[2])); }}' %s", derive_max, filename))

top3_min = max_activity - 1 * 24 * 3600
top3_min = top3_min < time_min ? time_min : top3_min
top3_max = max_activity + 3 * 24 * 3600
top3_max = top3_max > time_max ? time_max : top3_max
top3_min = system(sprintf("date -d @%d +'%s'", top3_min, "%Y-%m-%dT%H:%M:%S"))
top3_max = system(sprintf("date -d @%d +'%s'", top3_max, "%Y-%m-%dT%H:%M:%S"))


set title sprintf("%s signatures/minute (derive, top 3 days)", prefix)
set xrange [top3_min:top3_max]
plot filename using 1:2 title 'Signatures/minute (derive)'
