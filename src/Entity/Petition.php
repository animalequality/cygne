<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Serializer\Annotation\Groups;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Petition
 *
 * @ORM\Table(name="petition")
 * @ORM\Entity(repositoryClass="App\Repository\PetitionRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Petition
{
    // Do not change these magic numbers. Existing petitions use them.
    // Ex: require only (firstName, lastName) == 1 + 2 == 3
    // Note that email is always required
    const REQUIRE_FIRSTNAME  = 1 << 0; //  1
    const REQUIRE_LASTNAME   = 1 << 1; //  2
    const REQUIRE_CITY       = 1 << 2; //  4
    const REQUIRE_POSTALCODE = 1 << 3; //  8
    const REQUIRE_COUNTRY    = 1 << 4; // 16
    const REQUIRE_RECAPTCHA  = 1 << 5; // 32

    const REQUIRE_ALL        = (1 << 16) - 1 ^ self::REQUIRE_RECAPTCHA; // 65503 == max(UNSIGNED SMALLINT) - 32

    public function __construct() {
        $this->signatures = new ArrayCollection();
    }

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer")
     * @Groups({"read"})
     */
    private $id;

    /**
     * Date when petition has been created.
     *
     * @ORM\Column(name="created_at", type="datetime")
     * @Groups({"read"})
     */
    private $createdAt;

    /**
     * Petition slug.
     *
     * @ORM\Column(name="slug", type="string", length=100, unique=TRUE)
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^[a-z\d\-]+$/", message="Petition slug must only contain a-z, 0-9 and the dash.")
     * @Groups({"read"})
     */
    private $slug;

    /**
     * Petition title.
     *
     * @ORM\Column(name="title", type="string", length=255)   
     * @Assert\NotBlank()
     * @Groups({"read"})
     */
    private $title;

    /**
     * Comma-separated list of email Twig template directories to prepend or namespace.
     * - Namespace:
     *   If starting with a @ element is considered a namespace to use.
     *   Namespace must be defined in config/packages/twig.yaml and template must use include|extends('@ns/...') notation.
     * - Path:
     *   In all other case, element is considered a path to append.
     *   Parameter placeholder are supported and expanded at runtime.
     *   If a path does not start with a "/" or a "%", it's considered relative %kernel.project_dir%
     *
     * @ORM\Column(name="email_template_paths", type="string", nullable=true, length=255, options={"comment":"Comma-separated email template Twig paths or namespace."})
     */
    private $emailTemplatePaths;

    /**
     * Mailing (Mailchimp) list id to which subscriptions should be sent.
     *
     * @ORM\Column(name="mailinglist_id", type="string", length=15, nullable=true, options={"comment":"The numeric mailinglist/Mailchimp list id"})
     * @Assert\Regex(pattern="/^[a-f\d]+$/", message="An hexadecimal MailChimp list-id.")
     */
    private $mailingListId;

    /**
     * MailChimp source for newsletter subscriptions (defaults to petition slug).
     *
     * @ORM\Column(name="mailchimp_source", type="string", length=100, nullable=true)
     * @Assert\Regex(pattern="/^[a-z\d\-]+$/", message="MailChimp Source must only contain a-z, 0-9 and the dash.")
     */
    private $mailchimpSource;

    /**
     * Comma-separated list of groups to which subscriptions should be associated during subscription.
     * Value is agnostic and only subject to synchronization interpretation (could be Salesforce, MailChimp, CiviCRM, ...)
     *
     * @ORM\Column(name="groups", type="string", length=255, nullable=true, options={"comment":"Comma-separated list of groups to subscribe to"})
     */
    private $groups;

    /**
     * Not part of the ORM
     */
    private $extra;

    /**
     * Not part of the ORM
     */
    private $messages;

    /**
     * Signature fields this petition requires.
     *
     * @ORM\Column(name="requirements", type="smallint", options={"unsigned":true, "comment":"Represents which signature fields this petition requires"})
     */
    private $requirements = self::REQUIRE_ALL;

    /**
     * @ORM\OneToMany(targetEntity="Signature", mappedBy="petition")
     */
    private $signatures;

    /**
     * @ORM\PrePersist
     */
    function onPrePersist()
    {
        $this->createdAt = $this->createdAt ? : new \DateTime('now');
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getMailingListId() {
        return $this->mailingListId;
    }

    public function setMailingListId($mailingListId) {
        $this->mailingListId = $mailingListId;
    }

    public function getMailchimpSource()
    {
        return $this->mailchimpSource;
    }

    public function setMailchimpSource($mailchimpSource)
    {
        $this->mailchimpSource = $mailchimpSource;
    }

    public function getMailchimpSourceOrSlug()
    {
        $mailchimpSource = $this->getMailchimpSource();
        if (empty($mailchimpSource)) {
            $mailchimpSource = sprintf('petition-cygne-%s', $this->getSlug());
        }
        return strtolower(trim($mailchimpSource));
    }

    public function getGroups(): array
    {
        return $this->groups ? explode(',', $this->groups) : [];
    }

    public function setGroups(array $groups)
    {
        $this->groups = implode(',', array_values(array_filter($groups)));
    }

    public function getEmailTemplatePaths()
    {
        return $this->emailTemplatePaths;
    }

    public function setEmailTemplatePaths($emailTemplatePaths)
    {
        $this->emailTemplatePaths = $emailTemplatePaths;
    }

    /**
     * @return array of additional parameters which exist alongside other petition fields.
     *         No assumption can be made about it except that it's an array.
     */
    public function getExtra(): ?array
    {
        return $this->extra;
    }

    public function setExtra(array $extra)
    {
        $this->extra = $extra;
    }

    /**
     * @return array of additional messages and their fields.
     *         The keys are the name of the variant, the values are usable in the Thank-You email
     *
     */
    public function getMessages(): ?array
    {
        return $this->messages;
    }

    public function setMessages(array $messages)
    {
        $this->messages = $messages;
    }

    public function getRequirements()
    {
        $r = [];
        if ($this->requirements & self::REQUIRE_FIRSTNAME)  $r[] = 'firstName';
        if ($this->requirements & self::REQUIRE_LASTNAME)   $r[] = 'lastName';
        if ($this->requirements & self::REQUIRE_CITY)       $r[] = 'city';
        if ($this->requirements & self::REQUIRE_POSTALCODE) $r[] = 'postalCode';
        if ($this->requirements & self::REQUIRE_COUNTRY)    $r[] = 'country';
        if ($this->requirements & self::REQUIRE_RECAPTCHA)  $r[] = 'recaptcha';
        return $r;
    }

    public function setRequirements(array $requirements)
    {
        /* Note that even if firstName, lastName or email are not set to REQUIRED (= OPTIONAL)
           Petition entity will still always enforce validation of these fields. */
        $r = 0;
        if (in_array('firstName',  $requirements)) $r |= self::REQUIRE_FIRSTNAME;
        if (in_array('lastName',   $requirements)) $r |= self::REQUIRE_LASTNAME;
        if (in_array('city',       $requirements)) $r |= self::REQUIRE_CITY;
        if (in_array('postalCode', $requirements)) $r |= self::REQUIRE_POSTALCODE;
        if (in_array('country',    $requirements)) $r |= self::REQUIRE_COUNTRY;
        if (in_array('recaptcha',  $requirements)) $r |= self::REQUIRE_RECAPTCHA;
        $this->requirements = $r ? : self::REQUIRE_ALL;
    }

    public function getSignatures()
    {
        return $this->signatures;
    }
}
