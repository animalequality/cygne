<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\GroupSequenceProviderInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Annotation\Context;

/**
 * @ORM\Table(
 *   name="signature",
 *   uniqueConstraints={
 *     @ORM\UniqueConstraint(name="email_petition_unique", columns={"petition_id", "email"})
 *   },
 *   indexes={
 *     @ORM\Index(name="status_idx", columns={"status"}),
 *     @ORM\Index(name="confirm_email_status_idx", columns={"confirm_email_status"}),
 *     @ORM\Index(name="confirm_email_id_idx", columns={"confirm_email_id"}),
 *     @ORM\Index(name="newsletter_status_idx", columns={"newsletter_status"}),
 *   }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\SignatureRepository")
 * @ORM\HasLifecycleCallbacks()
 * @Assert\GroupSequenceProvider()
 */
class Signature implements GroupSequenceProviderInterface
{

    public const EMAIL_TODO = 'todo';
    public const EMAIL_SENT = 'sent'; // used to be 'done'
    public const EMAIL_DELIVERED = 'delivered';
    public const EMAIL_ERROR = 'error';
    public const EMAIL_IGNORE = 'ignore';

    /**
     * This property controls whether city, postalCode and country must be validated.
     * It's initialized by the SignatureController according to current petition requirements.
     * then getGroupSequence() use it to define which group-validators must be triggered
     */
    private $required = ['city', 'postalCode', 'country'];

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer", options={"comment":"unique signature identifier"})
     * @Groups({"read"})
     */
    private $id;

    /**
     * @ORM\Column(name="created_at", type="datetime", options={"comment":"time when the signature was created"})
     * @Groups({"read"})
     */
    private $createdAt;

    /**
     * @var datetime $updated
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true, options={"comment":"time when the signature was updated"})
     * @Groups({"read"})
     */
    protected $updatedAt;

    /**
     * @ORM\Column(name="revoked_at", type="datetime", nullable=true, options={"comment":"time when the signature was revoked/email has bounced (if applicable)"})
     * @Groups({"read"})
     */
    private $revokedAt;

    /**
     * @ORM\Column(name="first_name", type="string", length=100, options={"comment":"signer's first name"})
     * @Assert\NotBlank(groups = {"needs_firstName"}, normalizer="trim")
     * @Groups({"read", "write"})
     */
    private $firstName;

    /**
     * @ORM\Column(name="last_name", type="string", length=100, options={"comment":"signer's last name"})
     * @Assert\NotBlank(groups = {"needs_lastName"}, normalizer="trim")
     * @Groups({"read", "write"})
     */
    private $lastName;

    /**
     * @ORM\Column(name="email", type="string", length=100, options={"comment":"signer's email address"})
     * @Assert\NotBlank(normalizer="trim")
     * @Assert\Email(mode="html5")
     * @Assert\Regex(pattern="/[#$%&'\\\/=?^`{|}~]/", match=false)
     * @Assert\Regex(pattern="/\+\+/", match=false)
     * @Assert\Regex(pattern="/\.\./", match=false)
     * @Assert\Regex(pattern="/\.@/", match=false)
     * @Groups({"read", "write"})
     */
    private $email;

    /**
     * @ORM\Column(name="city", type="string", length=100, nullable=true, options={"comment":"signer's city"})
     * @Assert\NotBlank(groups = {"needs_city"}, normalizer="trim")
     * @Groups({"read", "write"})
     */
    private $city;

    /**
     * @ORM\Column(name="postal_code", type="string", length=30, nullable=true, options={"comment":"signer's postal code"})
     * @Assert\NotBlank(groups = {"needs_postalCode"}, normalizer="trim")
     * @Groups({"read", "write"})
     */
    private $postalCode;

    /**
     * @ORM\Column(name="country", type="string", length=2, nullable=true, options={"comment":"signer's country (ISO code)"})
     * @Assert\NotBlank(groups = {"need_country"})
     * @Assert\Regex("/^[A-Z]{2}$/", groups = {"needs_country"})
     * @Groups({"read", "write"})
     */
    private $country;

    /**
     * Comma-separated user languages as provided by Request::getLanguages() and derived from HTTP Accept-Language.
     * Sample value : en_US,es_MX,fr,de
     *
     * @ORM\Column(name="languages", type="string", length=32, nullable=true, options={"comment":"Comma-separated list of locales."})
     * @Groups({"read", "write"})
     */
    private $languages;

    /**
     * @ORM\Column(name="ip_address", type="string", length=255, options={"comment":"signer's IP address (IPv4 or IPv6)"})
     * @Assert\NotBlank()
     * @Assert\Ip(version="all")
     */
    private $ipAddress;

    /**
     * @ORM\Column(name="status", type="string", length=20, options={"default":"accepted"}, options={"comment":"status of the signature"})
     * @Assert\NotBlank()
     * @Assert\Choice(choices={"accepted", "revoked", "bounced", "invalid"})
     * @Groups({"read"})
     * `accepted` (default), the signature has been confirmed by our server
     * `revoked`: the user has asked us to revoke their signature
     * `invalid`: invalidated as spam/bot/fake/...
     * `bounced`: the confirmation email has bounced
     */
    private $status;

    /**
     * @ORM\Column(name="confirm_email_status", type="string", length=20, options={"default":"todo"}, options={"comment":"status of the confirmation email"})
     * @Assert\Choice(choices={"todo", "sent", "delivered", "error", "ignore"})
     * `todo` (default): email has not yet been sent
     * `sent`: email has been sent (used to be "done")
     * `delivered`: email has been received (SNS confirmation)
     * `error`: failed to send email
     * `ignore`: for some unknown reason, we chose to ignore this email
     */
    private $confirmEmailStatus;

    /**
     * @ORM\Column(name="confirm_email_id", type="string", length=255, nullable=true, options={"comment":"MessageId of the confirmation email"})
     */
    private $confirmEmailId;

    /**
     * @ORM\Column(name="confirm_email_error", type="string", length=255, nullable=true, options={"comment":"confirmation email error"})
     */
    private $confirmEmailError;

    /**
     * @ORM\Column(name="newsletter_status", type="string", length=20, options={"default":"todo"}, options={"comment":"status of the newsletter registration"})
     * @Assert\Choice(choices={"todo", "done", "error", "ignore"})
     * `todo` (default): signer has approved the subscription to the newsletter, but it hasn't yet been processed
     * `done`: subscription has been successfully completed
     * `error`: subscription has failed
     * `ignore`: signer has rejected to receive the newsletter or we just chose to ignore this subscription
     */
    private $newsletterStatus;

    /**
     * @ORM\Column(name="newsletter_error", type="string", length=255, nullable=true, options={"comment":"newsletter registration email error"})
     */
    private $newsletterError;

    /**
     * @ORM\Column(name="extra", type="text", nullable=true, options={"comment":"extra opaque JSON-encoded payload processed by synchronizer"})
     */
    private $extra;

    /**
     * @ORM\ManyToOne(targetEntity="Petition", inversedBy="signatures")
     * @ORM\JoinColumn(name="petition_id", referencedColumnName="id")
     * @Assert\NotNull()
     * @Groups({"read"})
     */
    private $petition;

    /**
     * @ORM\PrePersist
     */
    function onPrePersist()
    {
        //using Doctrine DateTime here
        $this->createdAt = new \DateTime('now');
    }

    /**
     * @ORM\PreUpdate
     */
    public function onPreUpdate()
    {
        $this->updatedAt = new \DateTime("now");
    }

    public function getId()
    {
        return $this->id;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getUpdatedAt(): ?DateTimeInterface
    {
        return $this->updatedAt ?: $this->createdAt;
    }

    public function getRevokedAt()
    {
        return $this->revokedAt;
    }

    public function setRevokedAt($revokedAt)
    {
        $this->revokedAt = $revokedAt;
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function setFirstName($firstName)
    {
        $this->firstName = trim($firstName);
    }

    public function getLastName()
    {
        return $this->lastName;
    }

    public function setLastName($lastName)
    {
        $this->lastName = trim($lastName);
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setEmail($email)
    {
        $this->email = trim($email);
    }

    public function getCity()
    {
        return $this->city;
    }

    public function setCity($city)
    {
        $this->city = trim($city);
    }

    public function getPostalCode()
    {
        return $this->postalCode;
    }

    public function setPostalCode($postalCode)
    {
        $this->postalCode = trim($postalCode);
    }

    public function getCountry()
    {
        return $this->country;
    }

    public function setCountry($country)
    {
        $this->country = trim($country);
    }

    public function getLanguages()
    {
        return $this->languages;
    }

    public function setLanguages($languages)
    {
        $this->languages = trim($languages);
    }

    public function getIpAddress()
    {
        return $this->ipAddress;
    }

    public function setIpAddress($ipAddress)
    {
        $this->ipAddress = $ipAddress;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getConfirmEmailStatus()
    {
        return $this->confirmEmailStatus;
    }

    public function setConfirmEmailStatus($confirmEmailStatus)
    {
        $this->confirmEmailStatus = $confirmEmailStatus;
    }

    public function getConfirmEmailId()
    {
        return $this->confirmEmailId;
    }

    public function setConfirmEmailId($confirmEmailId)
    {
        $this->confirmEmailId = (string)$confirmEmailId;
    }

    public function getConfirmEmailError()
    {
        return $this->confirmEmailError;
    }

    public function setConfirmEmailError($confirmEmailError)
    {
        $this->confirmEmailError = trim(substr((string)$confirmEmailError, 0, 255));
    }

    public function getNewsletterStatus()
    {
        return $this->newsletterStatus;
    }

    public function setNewsletterStatus($newsletterStatus)
    {
        $this->newsletterStatus = $newsletterStatus;
    }

    public function getNewsletterError()
    {
        return $this->newsletterError;
    }

    public function setNewsletterError($newsletterError)
    {
        $this->newsletterError = substr($newsletterError, 0, 255);
    }

    /**
     * @return array of additional parameters to be stored along signature fields.
     *         No assumption can be made about it except that:
     *         1. It's a json-encoded array.
     *         2. Keys' ordering is not preserved.
     *         3. Duplicated keys' are not preserved.
     *         4. It's serialized-form fits under 4k characters or is otherwise truncated.
     */
    public function getExtra(bool $keys_only = false): array
    {
        $json = @json_decode($this->extra ?? 'false', true);
        return $json ? ($keys_only ? array_keys($json) : (array)$json) : [];
    }

    public function setExtra(array $extra)
    {
        $this->extra = @json_encode($extra);
    }

    public function getPetition()
    {
        return $this->petition;
    }

    public function setPetition($petition)
    {
        $this->petition = $petition;
        $this->setRequirementsFromPetition($petition);
    }

    private function setRequirementsFromPetition($petition)
    {
        $requirements = $petition->getRequirements();
        if (!empty($requirements)) {
            $this->required = $requirements;
        }
        // In the (unexpected) event that petition would return no requirement
        // assume it's a bug (like setting 0 in petition `requirements` column) and ignore.
    }

    /**
     * @return GroupSequence|array
     */
    public function getGroupSequence()
    {
        // "Signature" is a "default" group, absolutely needed in order to validator all constraints without a specific group
        $validator_groups = [ 'Signature' ];
        if (in_array('firstName',  $this->required)) $validator_groups[] = 'needs_firstName';
        if (in_array('lastName',   $this->required)) $validator_groups[] = 'needs_lastName';
        if (in_array('city',       $this->required)) $validator_groups[] = 'needs_city';
        if (in_array('postalCode', $this->required)) $validator_groups[] = 'needs_postalCode';
        if (in_array('country',    $this->required)) $validator_groups[] = 'needs_country';

        return [ $validator_groups ];
    }
}
