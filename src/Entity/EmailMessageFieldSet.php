<?php

namespace App\Entity;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * Thank-You email message
 */
class EmailMessageFieldSet
{

    /**
     * Petition title.
     * Per-message title is optional (but strongly advised)
     * because a per-petition title is already mandatory.
     *
     * ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * Optional per-message template overriding petition's fixed one.
     *
     * Comma-separated list of email Twig template directories to prepend or namespace.
     * - Namespace:
     *   If starting with a @ element is considered a namespace to use.
     *   Namespace must be defined in config/packages/twig.yaml and template must use include|extends('@ns/...') notation.
     * - Path:
     *   In all other case, element is considered a path to append.
     *   Parameter placeholder are supported and expanded at runtime.
     *   If a path does not start with a "/" or a "%", it's considered relative %kernel.project_dir%
     *
     * ORM\Column(name="email_template_paths", type="string", nullable=true, length=255, options={"comment":"Comma-separated email template Twig paths or namespace."})
     */
    private $emailTemplatePaths;

    /**
     * Petition subtitle (used in emails).
     *
     * ORM\Column(name="subtitle", type="text", nullable=true)
     */
    private $subtitle;

    /**
     * Name of the email sender from which confirmation emails will be sent
     *
     * ORM\Column(name="email_from_name", type="string", length=100)
     * @Assert\NotBlank()
     */
    private $emailFromName;

    /**
     * The email address from which confirmation emails will be sent
     *
     * ORM\Column(name="email_from_email", type="string", length=100)   
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $emailFromEmail;

    /**
     * Reply-To field for confirmation emails
     *
     * ORM\Column(name="email_reply_to", type="string", length=100)   
     * @Assert\NotBlank()
     * @Assert\Email()
     */
    private $emailReplyTo;

    /**
     * Email subject for confirmation emails.
     *
     * ORM\Column(name="email_subject", type="string", length=255, nullable=true)
     */
    private $emailSubject;

    /**
     * Donation campaign for links (defaults to "{year}/{slug}").
     *
     * ORM\Column(name="donation_campaign", type="string", length=100, nullable=true)
     * @Assert\Regex(pattern="/^[a-z\d\-\/]+$/", message="Donation campaigns must only contain a-z, 0-9, the dash and the slash.")
     */
    private $donationCampaign;

    /**
     * Public link to share the petition.
     * Mandatory. Used for redirection and thank-you emails.
     *
     * @Assert\NotBlank()
     */
    private $formPageUrl;

    /**
     * Public URL for Twitter sharing buttons.
     *
     * ORM\Column(name="share_twitter_url", type="text")
     */
    private $shareTwitterUrl;

    /**
     * Public URL for Facebook sharing buttons.
     *
     * ORM\Column(name="share_facebook_url", type="text")
     */
    private $shareFacebookUrl;

    /**
     * Not part of the ORM
     */
    private $extra;

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getEmailTemplatePaths()
    {
        return $this->emailTemplatePaths;
    }

    public function setEmailTemplatePaths($emailTemplatePaths)
    {
        $this->emailTemplatePaths = $emailTemplatePaths;
    }

    public function getSubtitle()
    {
        return $this->subtitle;
    }

    public function setSubtitle($subtitle)
    {
        $this->subtitle = $subtitle;
    }

    public function getEmailFromName()
    {
        return $this->emailFromName;
    }

    public function setEmailFromName($emailFromName)
    {
        $this->emailFromName = $emailFromName;
    }

    public function getEmailFromEmail()
    {
        return $this->emailFromEmail;
    }

    public function setEmailFromEmail($emailFromEmail)
    {
        $this->emailFromEmail = $emailFromEmail;
    }

    public function getEmailReplyTo()
    {
        return $this->emailReplyTo;
    }

    public function setEmailReplyTo($emailReplyTo)
    {
        $this->emailReplyTo = $emailReplyTo;
    }

    public function getEmailSubject()
    {
        return $this->emailSubject;
    }

    public function setEmailSubject($emailSubject)
    {
        $this->emailSubject = $emailSubject;
    }

    public function getDonationCampaign()
    {
        return $this->donationCampaign;
    }

    public function setDonationCampaign($donationCampaign)
    {
        $this->donationCampaign = $donationCampaign;
    }

    public function getDonationCampaignOrSlug()
    {
        return strtolower(trim($this->getDonationCampaign()));
        if (empty($donationCampaign)) {
            // $donationCampaign = sprintf('%s/%s', $this->getCreatedAt()->format('Y'), $this->getSlug());
        }
    }

    public function getFormPageUrl()
    {
        return $this->formPageUrl;
    }

    public function setFormPageUrl($formPageUrl)
    {
        $this->formPageUrl = $formPageUrl;
    }

    public function getShareTwitterUrl()
    {
        return $this->shareTwitterUrl;
    }

    public function setShareTwitterUrl($shareTwitterUrl)
    {
        $this->shareTwitterUrl = $shareTwitterUrl;
    }

    public function getShareFacebookUrl()
    {
        return $this->shareFacebookUrl;
    }

    public function setShareFacebookUrl($shareFacebookUrl)
    {
        $this->shareFacebookUrl = $shareFacebookUrl;
    }

    /**
     * @return array of additional parameters which exist alongside other petition fields.
     *         No assumption can be made about it except that it's an array.
     */
    public function getExtra(): ?array
    {
        return $this->extra;
    }

    public function setExtra(?array $extra)
    {
        $this->extra = $extra;
    }
}
