<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * The content of this table comes from previous database dumps.
 * The purpose is to be able to identify duplicated signature without keeping a huge amount of personal data in the database.
 * A salted hash is replacing the email which can be used by the SignatureController to identify the previous existence of this signature.
 * A way to import/refresh the table is:
 * INSERT IGNORE INTO obfuscated_signature (petition_id, hashed_email) SELECT petition_id, MD5(CONCAT('prefix', email)) from signature;
 * or, if coming from a file generated from `SELECT petition_id, MD5(CONCAT('prefix', email)) from signature INTO OUTFILE ...`
 * then use `LOAD DATA LOCAL INFILE 'foo.csv' IGNORE INTO TABLE obfuscated_signature FIELDS TERMINATED BY '\t' (petition_id, hashed_email);`
 *
 * @ORM\Table(
 *   name="obfuscated_signature",
 *   uniqueConstraints={
 *     @ORM\UniqueConstraint(name="obfuscated_email_petition_unique", columns={"petition_id", "hashed_email"})
 *   }
 * )
 * @ORM\Entity(repositoryClass="App\Repository\ObfuscatedSignatureRepository")
 */
class ObfuscatedSignature
{

    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(name="id", type="integer", options={"comment":"unique obfuscated signature identifier"})
     */
    private $id;

    /**
     * @ORM\Column(name="hashed_email", type="string", length=32, options={"fixed":true,"comment":"encrypted email address"})
     */
    private $hashedEmail;

    /**
     * @ORM\ManyToOne(targetEntity="Petition", inversedBy="signatures")
     * @ORM\JoinColumn(name="petition_id", referencedColumnName="id")
     */
    private $petition;

    public function getHEmail()
    {
        return $this->hashedEmail;
    }

    public function getPetition()
    {
        return $this->petition;
    }
}
