<?php

namespace App\Listener;

use Noxlogic\RateLimitBundle\Events\GenerateKeyEvent;

class RateLimitGenerateKeyListener
{
    public function onGenerateKey(GenerateKeyEvent $event)
    {
        $key = $this->generateKey($event);
        $event->addToKey($key);
    }

    private function generateKey(GenerateKeyEvent $event) {
        $request = $event->getRequest();
        return $request->getClientIp();
    }
}
