<?php

namespace App\Encoder;

use Symfony\Component\Serializer\Encoder\EncoderInterface;
use Symfony\Component\Serializer\Encoder\DecoderInterface;

/**
 * Decodes Form URL-Encoded data.
 */
class FormEncoder implements EncoderInterface, DecoderInterface
{
    const FORMAT = 'form';

    /**
     * Constructs a new FormDecode instance.
     */
    public function __construct()
    {
        //
    }

    /**
     * {@inheritdoc}
     */
    public function encode($data, $format, array $context = []): string
    {
        throw new \Exception("Not implemented");
    }

    /**
     * {@inheritdoc}
     */
    public function decode($data, $format, array $context = [])
    {
        parse_str($data, $decodedData);
        return $decodedData;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsEncoding($format): bool
    {
        return self::FORMAT === $format;
    }

    /**
     * {@inheritdoc}
     */
    public function supportsDecoding($format): bool
    {
        return self::FORMAT === $format;
    }
}
