<?php

namespace App\Encoder;

use Symfony\Component\Serializer\Annotation\SerializedName;

/**
 * This class allows the deserialization of parameters that are not
 * in App\Entity\Signature but need to be accessed by the Signature API.
 */
class SignatureObject
{
    private $newsletter;

    /**
     * @SerializedName("g-recaptcha-response")
     */
    private $recaptchaToken  = null;
    private $redirect        = null;
    private $redirectSuccess = null;
    private $redirectError   = null;
    private $molassespot     = null;

    public function getNewsletter() {
        return $this->newsletter;
    }

    public function setNewsletter($newsletter) {
        if (is_string($newsletter)) {
            $newsletter = strtolower($newsletter) === 'true' || strtolower($newsletter) === '1';
        }
        $this->newsletter = $newsletter;
    }

    public function getRecaptchaToken() {
        return $this->recaptchaToken;
    }

    public function setRecaptchaToken($recaptchaToken) {
        $this->recaptchaToken = $recaptchaToken;
    }

    public function getRedirect() {
        return $this->redirect;
    }

    public function setRedirect($redirect) {
        $this->redirect = $redirect;
    }

    public function getRedirectSuccess() {
        return $this->redirectSuccess;
    }

    public function setRedirectSuccess($redirectSuccess) {
        $this->redirectSuccess = $redirectSuccess;
    }

    public function getRedirectError() {
        return $this->redirectError;
    }

    public function setRedirectError($redirectError) {
        $this->redirectError = $redirectError;
    }

    public function getMolassesPot() {
        return $this->molassespot;
    }

    public function setMolassesPot($molassespot) {
        $this->molassespot = $molassespot;
    }
}
