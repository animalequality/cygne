<?php

namespace App\Traits;

use Doctrine\ORM\Proxy\Proxy;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\PropertyAccess\PropertyAccess;

trait UnitOfWorkTrait
{
    /**
     * Tests if an entity is loaded - must either be a loaded proxy or not a proxy
     *
     * @param object $entity
     *
     * @return bool
     */
    private function isLoaded($entity)
    {
        return !($entity instanceof Proxy) || $entity->__isInitialized();
    }

    /**
     * Kind of replacement for the (obsolete) EntityInterface::merge()
     * Removed in Doctrine ORM 3.0
     * See https://github.com/doctrine/orm/pull/1577/files#diff-6e8c1c1e78b054ba05e20ea09d877865L3289
     *
     * @param object $entity
     * @param object $managedCopy
     *
     * @throws ORMException
     * @throws OptimisticLockException
     * @throws TransactionRequiredException
     */
    private function mergeEntityStateIntoManagedCopy($entity, $managedCopy)
    {
        $class = $this->em->getClassMetadata(get_class($entity));

        if ($managedCopy === null) {
            throw EntityNotFoundException::fromClassNameAndIdentifier(
                get_class($entity),
                [get_class($entity) => $entity->getId()]
            );
        }

        if (! $this->isLoaded($entity)) {
            return;
        }

        if (! $this->isLoaded($managedCopy)) {
            $managedCopy->__load();
        }

        $propertyAccessor = PropertyAccess::createPropertyAccessorBuilder()->getPropertyAccessor();
        $changes = [];

        foreach ($class->fieldMappings as $property) {
            $vOld = $propertyAccessor->getValue($managedCopy, $property['columnName']);
            $vNew = $propertyAccessor->getValue($entity, $property['columnName']);

            $diff = $vOld !== $vNew;
            if ($property['type'] === 'datetime') {
                // $diff = $vOld->getTimestamp() - $vNew->getTimestamp() != 0;
                $diff = $vOld->format('Y-m-d\TH:i:s') !== $vNew->format('Y-m-d\TH:i:s');
            }

            if ($diff) {
                $propertyAccessor->setValue($managedCopy, $property['columnName'], $vNew);
                // No used
                $changes[] = [$property['columnName'] => [$vOld, $vNew]];
            }
        }
    }
}
