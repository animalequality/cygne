<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Output\OutputInterface;

use App\Entity\Signature;
use App\Entity\Petition;
use App\Repository\SignatureRepository;

class PetitionCountAdd extends Command
{
    protected static $defaultName = 'petition:count-add';
    private $em;
    private $signature_repository;

    public function __construct(EntityManagerInterface $em, SignatureRepository $signature_repository)
    {
        $this->em = $em;
        $this->signature_repository = $signature_repository;
        parent::__construct();
    }

    protected function configure()
    {
        $this
          ->addArgument('petition-id', InputArgument::REQUIRED, 'Petition identifier')
          ->addArgument('count', InputArgument::REQUIRED, 'Number of signatures to add (0 to delete the count override file)')
          ->addOption('archived', '-a', InputOption::VALUE_NONE, "Account for archived signatures too")
          ->setDescription('Create or delete signature-counting file-based overrides for specified petitions.')
          ->setHelp('Cygne allows overriding signature total using a regular file within %kernel.cache_dir%.'
                    . PHP_EOL
                    . 'This command adds a constant number to the total of signatures of a petition');
    }

    protected function unlink(string $filename, SymfonyStyle $io)
    {
        if (file_exists($filename)) {
            if (unlink($filename)) {
                $io->text(sprintf("Remove override file %s.", $filename));
            } else {
                $io->text(sprintf("Could not remove override file %s.", $filename));
            }
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $id = intval($input->getArgument('petition-id'));
        $count = intval($input->getArgument('count'));
        $filename = $this->signature_repository->getCountOverrideFilename($id);
        $io = new SymfonyStyle($input, $output);

        if ($count === 0) {
            $this->unlink($filename, $io);
            return 0;
        }

        // Check if petition with given id exists
        $idCount = $this->em->createQueryBuilder()
                            ->from(Petition::class, 'p', 'p.id')
                            ->select('count(p.id)')
                            ->andWhere('p.id = :petition_id')
                            ->setParameter('petition_id', $id)
                            ->getQuery()
                            ->getSingleScalarResult();
        if (intval($idCount) !== 1) {
            throw new \RuntimeException('Invalid petition id');
        }

        $query = $this->em->createQueryBuilder()
                          ->from(Signature::class, 's', 's.status')
                          ->select('s.status', 'COUNT(s.id) as c')
                          ->andWhere('s.petition = :petition_id')
                          ->groupBy('s.status')
                          ->setParameter('petition_id', $id)
                          ->getQuery();
        $result = $query->getArrayResult();

        $archived_count = 0;
        if ($input->getOption('archived')) {
            $archived_count = $this->signature_repository->getArchivedSignatureCount([$id]);
        }


        $content = (int)($result['accepted']['c'] ?? 0) + $archived_count + $count;
        if (isset($result['bounced'])) {
            $content .= ':' . (int)$result['bounced']['c'];
            if (isset($result['revoked'])) {
                $content .= ':' . (int)$result['revoked']['c'];
            }
        }

        if ($content) {
            $wb = file_put_contents($filename, $content);
            $io->text(sprintf('Wrote "%s" into override file %s.', $content, $filename));
        } else {
            // Remove an existing file if no good data could be gathered
            $this->unlink($filename, $io);
        }

        return 0;
    }
}
