<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

use App\Entity\Petition;
use App\Controller\PetitionController;

class PetitionCreateCommand extends Command
{
    protected static $defaultName = 'petition:create';
    private $translator;
    private $petition_controller;
    private $params;

    public function __construct(TranslatorInterface $translator, ParameterBagInterface $params, PetitionController $petition_controller)
    {
        $this->translator = $translator;
        $this->petition_controller = $petition_controller;
        $this->params = $params;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Create a new petition object');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $data = [];
        $default_listid = $this->params->get('default_mailinglist_id') ? : '';

        $io->text('Create a new petition');
        $data['slug']               = $io->ask('Petition slug');
        $data['title']              = $io->ask('Petition title');
        $data['subtitle']           = $io->ask('Petition subtitle (used in confirmation emails)');

        $io->text('Petition MailChimp source');
        $data['mailchimpSource']    = $io->ask('MailChimp "source" param (defaults to "petition-cygne-[slug]")');
        $data['mailingListId']      = $io->ask(sprintf('Mailing-list ID (defaults to "%s")', $default_listid), $default_listid);
        $data['groups']    = $io->ask('Comma-separated groups');
        $data['groups']    = explode(',', $data['groups']);

        $io->text('Petition email settings');
        $data['emailFromName']      = $io->ask('Email from name', 'Example');
        $data['emailFromEmail']     = $io->ask('Email from address', 'ne-pas-repondre@example.com');
        $data['emailReplyTo']       = $io->ask('Email Reply-To field', 'ne-pas-repondre@example.com');
        $data['emailSubject']       = $io->ask('Email subject', $this->translator->trans('Your Petition Signature'));

        $io->text('Petition email content');
        $data['formPageUrl']       = $io->ask('Email field "formPageUrl"', '');
        $data['shareTwitterUrl']    = $io->ask('Email field "shareTwitterUrl"', '');
        $data['shareFacebookUrl']   = $io->ask('Email field "shareFacebookUrl"', '');

        $petition = $this->petition_controller->createPetition($data);
        $io->success('Petition Created ID: ' . $petition->getId());

        return 0;
    }
}
