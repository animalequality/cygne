<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Petition;
use App\Entity\Signature;

class PetitionListCommand extends Command
{
    protected static $defaultName = 'petition:list';
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('List petition objects');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $petitions = $this->em->getRepository(Petition::class)->findAll();
        $repository = $this->em->getRepository(Signature::class);

        $rows = array_map(function ($petition) use ($repository) {
            return [
                $petition->getId(),
                $petition->getCreatedAt()->format('Y-m-d'),
                $petition->getTitle(),
                $repository->getSignatureCount($petition),
            ];
        }, $petitions);

        $io->table(['ID', 'Creation', 'Title', 'Signatures'], $rows);

        return 0;
    }
}
