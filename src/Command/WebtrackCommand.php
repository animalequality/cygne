<?php

namespace App\Command;

use App\Repository\SignatureRepository;
use Psr\Log\LoggerInterface;

class WebtrackCommand extends \Cygne\CygneSyncBundle\Command\BaseWebtrackCommand {
    protected static $defaultName = 'cygne:webtrack-cygne';
    private SignatureRepository $signatureRepository;

    public function __construct(LoggerInterface $logger, array $metaConversionApiConfig, SignatureRepository $signatureRepository) {
        $this->signatureRepository = $signatureRepository;

        parent::__construct($logger, $metaConversionApiConfig);
    }

    protected function createMetaConversionApiPayload(int $id, string $userAgent): array {
        $signature = $this->signatureRepository->findOneBy(['id' => $id]);
        $extra = $signature->getExtra();

        $payload = [
            'event_name' => $signature->getNewsletterStatus() === 'todo' ? 'Lead' : 'CompleteRegistration',
            'action_source' => 'website',
            'event_time' => time(),
            'user_data' => [
                'client_user_agent' => $userAgent,
                'client_ip_address' => $signature->getIpAddress(),
                'em' => hash('sha256', strtolower(trim($signature->getEmail()))),
                'fn' => hash('sha256', strtolower($signature->getFirstName())),
                'ln' => hash('sha256', strtolower($signature->getLastName())),
            ]
        ];

        if (!empty($extra['eventId'])) {
            $payload['event_id'] = $extra['eventId'];
        }

        if (!empty($extra['fbc'])) {
            $payload['user_data']['fbc'] = $extra['fbc'];
        }

        if (!empty($extra['fbp'])) {
            $payload['user_data']['fbp'] = $extra['fbp'];
        }

        return $payload;
    }

    protected function getPetitionOrGroup(int $id): int
    {
        $signature = $this->signatureRepository->findOneBy(['id' => $id]);

        return $signature->getPetition()->getId();
    }
}
