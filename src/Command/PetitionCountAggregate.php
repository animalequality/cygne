<?php

namespace App\Command;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Console\Output\OutputInterface;

use App\Entity\Signature;
use App\Entity\Petition;
use App\Repository\SignatureRepository;

class PetitionCountAggregate extends Command
{
    protected static $defaultName = 'petition:count-aggregate';
    private $em;
    private $signature_repository;

    public function __construct(EntityManagerInterface $em, SignatureRepository $signature_repository)
    {
        $this->em = $em;
        $this->signature_repository = $signature_repository;
        parent::__construct();
    }

    protected function configure()
    {
        $this
          ->addOption('delete', '-D', InputOption::VALUE_NONE, "Instead of creating, delete the count override file")
          ->addOption('archived', '-a', InputOption::VALUE_NONE, "Account for archived signatures too")
          ->addOption('add', null, InputOption::VALUE_REQUIRED, 'Number of signatures to artificially add', 0)
          ->addOption('show', null, InputOption::VALUE_REQUIRED, 'Only show the result instead of creating a file', false)
          ->addArgument('petition-id', InputArgument::REQUIRED | InputArgument::IS_ARRAY, 'Petition identifiers')
          ->setDescription('Create or delete signature-counting file-based overrides for specified petitions.')
          ->setHelp('Cygne allows overriding signature total using a regular file within %kernel.cache_dir%.'
                    . PHP_EOL
                    . 'This command sums the total of accepted/bounced/revoked/invalid signatures for all petitions passed on'
                    . ' the command-line and creates corresponding override files.');
    }

    protected function unlink(string $filename, SymfonyStyle $io)
    {
        if (file_exists($filename)) {
            if (unlink($filename)) {
                $io->text(sprintf("Remove override file %s.", $filename));
            } else {
                $io->text(sprintf("Could not remove override file %s.", $filename));
            }
        }
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $ids = array_filter(array_map('intval', $input->getArgument('petition-id')));
        $add = intval($input->getOption('add'));
        $io = new SymfonyStyle($input, $output);
        if ($input->getOption('delete')) {
            foreach ($ids as $id) {
                $filename = $this->signature_repository->getCountOverrideFilename($id);
                $this->unlink($filename, $io);
            }
            return 0;
        }

        if (count($ids) < 2) {
            throw new \RuntimeException('At least two petitions ids must be specified.');
        }

        // Filter-out non-existing petition ids (avoid creating misleading override files)
        $real_ids = $this->em->createQueryBuilder()
                            ->from(Petition::class, 'p', 'p.id')
                            ->select('p.id')
                            ->andWhere('p.id IN (:petition_ids)')
                            ->setParameter('petition_ids', $ids)
                            ->getQuery()
                            ->getResult();
        $ids = array_keys($real_ids);

        $query = $this->em->createQueryBuilder()
                       ->from(Signature::class, 's', 's.status')
                       ->select('s.status', 'COUNT(DISTINCT LOWER(CASE WHEN (s.email LIKE :pattern) THEN REPLACE(s.email, :dot, :empty) ELSE s.email END)) as c')
                       ->andWhere('s.petition IN (:petition_ids)')
                       ->groupBy('s.status')
                       ->setParameter('pattern', '%@gmail.com')
                       ->setParameter('dot', '.')
                       ->setParameter('empty', '')
                       ->setParameter('petition_ids', $ids)
                       ->getQuery();

        $result = $query->getArrayResult() + ['accepted' => ['c' => 0]];

        $archived_count = 0;
        if ($input->getOption('archived')) {
            $archived_count = $this->signature_repository->getArchivedSignatureCount($ids);
        }

        $content = (int)$result['accepted']['c'] + $archived_count + $add;
        if (isset($result['bounced'])) {
            $content .= ':'
                     . (int)$result['bounced']['c']
                     . (isset($result['revoked']) ? (':' . (int)$result['revoked']['c']) : '')
                     . (isset($result['invalid']) ? (':' . (int)$result['invalid']['c']) : '');
        }

        if ($input->getOption('show')) {
            echo $content;
            return 0;
        }

        foreach ($ids as $id) {
            $filename = $this->signature_repository->getCountOverrideFilename($id);
            if ($content) {
                $wb = file_put_contents($filename, $content);
                $io->text(sprintf('Wrote "%s" into override file %s.', $content, $filename));
            } else {
                // Remove an existing file if no good data could be gathered
                $this->unlink($filename, $io);
            }
        }

        return 0;
    }
}
