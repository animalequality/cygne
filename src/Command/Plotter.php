<?php

namespace App\Command;

use App\Controller\GraphController;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class Plotter extends Command
{
    protected static $defaultName = 'petition:graph';
    private $gc;

    public function __construct(GraphController $gc)
    {
        $this->gc = $gc;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Generate graph or statistics files for given petition(s).')
             ->addArgument('petitions', InputArgument::OPTIONAL | InputArgument::IS_ARRAY, 'Petition ID or slug to select. Leave blank for all.')
             ->addOption('output-directory', '', InputOption::VALUE_REQUIRED, 'Output directory.', $this->gc->outputDirectory)
             ->addOption('gnuplot-script', '', InputOption::VALUE_REQUIRED, 'Gnuplot script.', $this->gc->plotterFilename)
             ->addOption('width', '', InputOption::VALUE_REQUIRED, 'Output graph width', $this->gc::DEFAULT_WIDTH)
             ->addOption('height', '', InputOption::VALUE_REQUIRED, 'Output graph height', $this->gc::DEFAULT_HEIGHT)
             ->addOption('stats-only', '', InputOption::VALUE_NONE, 'Only output stat files but no Gnuplot graphs');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $petitionSelector = array_filter($input->getArgument('petitions'));
        $this->gc->setPlotterFilename($input->getOption('gnuplot-script'));
        $this->gc->setOutputDirectory($input->getOption('output-directory'));

        $petitions = $this->gc->getPetitions($petitionSelector);
        $this->gc->generateGraphs(
            $petitions,
            intval($input->getOption('width')),
            intval($input->getOption('height'))
        );

        return 0;
    }
}
