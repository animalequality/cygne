<?php

namespace App\Command;

use App\Entity\Petition;
use App\Repository\YamlPetitionRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Doctrine\ORM\Id\AssignedGenerator;
use Doctrine\ORM\Mapping\ClassMetadata;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Yaml\Yaml;

class PetitionSyncFromYaml extends Command
{
    use \App\Traits\UnitOfWorkTrait;

    protected static $defaultName = 'petition:sync-from-yaml';
    private $em;
    private $yamlRepository;

    public function __construct(YamlPetitionRepository $yamlRepository, EntityManagerInterface $em)
    {
        $this->yamlRepository = $yamlRepository;
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Import petitions stored in a Yaml file into the database.')
             ->addArgument('files', InputArgument::REQUIRED | InputArgument::IS_ARRAY, 'Yaml files to load.')
             ->addOption('force', 'f', InputOption::VALUE_NONE, 'Do actual changes.')
             ->addOption('accept-id-change', '', InputOption::VALUE_NONE, 'Accept unique id (primary key)' .
                         ' modification for already existing slugs (Dangerous).');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->yamlRepository->setConfig($input->getArgument('files') ?? [], false);
        $yaml_petitions = $this->yamlRepository->findAll();

        $io = new SymfonyStyle($input, $output);
        if (! $yaml_petitions) {
            $io->error('No petition found in the Yaml file');
            return 1;
        }

        /**
         * Ensures that the same numeric ID is used.
         */
        $metadata = $this->em->getClassMetaData(Petition::class);
        $metadata->setIdGeneratorType(ClassMetadata::GENERATOR_TYPE_NONE);
        $metadata->setIdGenerator(new AssignedGenerator());

        $uow = $this->em->getUnitOfWork();

        foreach ($this->em->getRepository(Petition::class)->findAll() as $p) {
            if (!$this->yamlRepository->find($p->getId())) {
                $uow->remove($p);
            }
        }

        // $db_petitions = [];
        foreach ($yaml_petitions as $id => $p) {
            try {
                $db_petition = $this->em->getRepository(Petition::class)->find($id);
                if (!$db_petition) {
                    $db_petition = $this->em->getRepository(Petition::class)->findOneBy(
                        ['slug' => $p->getSlug()]
                    );
                    if ($db_petition) {
                        $warning = sprintf(
                            'Found that slug "%s" already exist under id %d instead of suggested %d.',
                            $p->getSlug(),
                            $db_petition->getId(),
                            $id
                        );

                        if (! $input->getOption('accept-id-change')) {
                            throw new \RuntimeException($warning . PHP_EOL . 'Use --accept-id-change to bypass.');
                        }

                        $io->warning($warning);
                    }
                }

                /**
                 * Below method compensate the deprecation of EntityManager::merge().
                 * Formerly: $db_petition = $this->em->merge($p);
                 */
                $this->mergeEntityStateIntoManagedCopy($p, $db_petition);
            } catch (EntityNotFoundException $e) {
                $this->em->persist($p);
            }
        }

        $uow->computeChangeSets();
        $changeset = [
            'deletion' => $uow->getScheduledEntityDeletions(),
            'update' => $uow->getScheduledEntityUpdates(),
            'insertion' => $uow->getScheduledEntityInsertions()
        ];

        foreach ($changeset as $todo => $items) {
            foreach ($items as $p) {
                $io->text(sprintf('Petition % 3d needs %s', $p->getId(), $todo));
                if ($input->getOption('verbose')) {
                    $io->text(Yaml::dump($uow->getEntityChangeSet($p)));
                }
            }
        }

        if (!$input->getOption('force')) {
            return 0;
        }

        $io->text(sprintf('Applying changes'));
        $this->em->flush();
        return 0;
    }
}
