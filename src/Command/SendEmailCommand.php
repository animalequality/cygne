<?php

namespace App\Command;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use App\Controller\ConfirmationController;
use App\Service\SpamChecker;
use App\Entity\Signature;
use App\Entity\Petition;

class SendEmailCommand extends Command
{
    protected static $defaultName = 'signatures:send-email';

    private $confirmation_controller;
    private $spam_checker;
    private $em;
    private $logger;

    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $em,
        ConfirmationController $confirmation_controller,
        SpamChecker $spam_checker
    ) {
        $this->logger = $logger;
        $this->em = $em;
        $this->confirmation_controller = $confirmation_controller;
        $this->spam_checker = $spam_checker;
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setDescription('Send confirmation email')
            ->addOption('limit', null, InputOption::VALUE_REQUIRED, 'Limit run to that many signature. Value of 0 (default) synchronizes ALL items having confirm_email_status `todo`')
            ->addOption('signature', null, InputOption::VALUE_REQUIRED, 'Limit run to these signatures only (ID or begining of email).')
            ->addOption('petition', null, InputOption::VALUE_REQUIRED, 'Limit run signature done for this petition ID.')
            ->addOption('spam-check', null, InputOption::VALUE_REQUIRED, 'Spam checking policy. One of "no", "soft" (aliased to "on") or "hard". The later fails if anti-spam is unavailable')
            ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Only show to whom emails would be sent.')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {

        $limit = is_numeric($input->getOption('limit')) ? (int)$input->getOption('limit') : null;
        $petitionId = is_numeric($input->getOption('petition')) ? (int)$input->getOption('petition') : null;
        $spam_check = $input->getOption('spam-check') ?: 'soft';
        $spam_check = in_array($spam_check, ['on', 'off', 'soft', 'hard']) ? $spam_check : 'soft';

        $criteria = Criteria::create()
                  ->where(Criteria::expr()->eq('confirmEmailStatus', Signature::EMAIL_TODO))
                  ->andWhere(Criteria::expr()->eq('status', 'accepted'))
                  ->orderBy(['id' => Criteria::ASC]);
        if (($sign = $input->getOption('signature'))) {
            if (is_numeric($sign)) {
                $criteria->andWhere(Criteria::expr()->eq('id', (int)$sign));
            } else {
                $criteria->andWhere(Criteria::expr()->startsWith('email', $sign));
            }
        }
        if ($petitionId) {
            $p = $this->em->getRepository(Petition::class)->findOneBy(['id' => $petitionId]);
            $criteria->andWhere(Criteria::expr()->eq('petition', $p));
        }
        if ($limit) {
            $criteria->setMaxResults($limit);
        }

        $signatures = $this->em->getRepository(Signature::class)->matching($criteria);
        if ($limit) {
            $signatures = $signatures->slice(0, $limit);
        }

        $spam_results = null;
        if ($spam_check !== 'off') {
            $to_check = array_unique(array_map(fn($e) => $e->getEmail(), is_array($signatures) ? $signatures : $signatures->toArray()));
            // Associative array. email as key, bool as whether these are spam
            $mode = SpamChecker::SOFT_CHECK | ($input->getOption('dry-run') ? SpamChecker::OFFLINE_CHECK : SpamChecker::FULL_CHECK);
            $spam_results = $this->spam_checker->verifyMultiple($to_check, $mode, $this->logger);
        }

        $spammy_signatures = $valid_signatures = [];
        if (array_sum($spam_results) > 0) { // at least one spam identified
            foreach ($signatures as $s) {
                if ($spam_results[$s->getEmail()]) {
                    $spammy_signatures[] = $s;
                } else {
                    $valid_signatures[] = $s;
                }
            }

            $this->logger->info(
                'Spam filtering: from {c1} email addresses down to {c2}',
                ['c1' => count($signatures), 'c2' => count($valid_signatures)]
            );
            $signatures = $valid_signatures;
        }

        $count = count($signatures);
        if ($input->getOption('dry-run')) {
            $output->writeln(sprintf('Confirmation emails for %d signatures [DRY-RUN]', $count));
            foreach ($signatures as $s) {
                $output->writeln(
                    sprintf(
                        '% 6d, petition % 2d: %s %s',
                        $s->getId(),
                        $s->getPetition()->getId(),
                        $s->getCreatedAt()->format('Y-m-d H:i:s'),
                        $s->getEmail()
                    )
                );
            }

            return 0;
        }

        if (count($spammy_signatures)) {
            // Classify as spam
            $logger = $this->logger;
            $this->em->transactional(function () use ($spammy_signatures, $spam_results, $logger) {
                $ids = [];
                foreach ($spammy_signatures as $s) {
                    $ids[] = $s->getId();
                    $s->setConfirmEmailStatus(Signature::EMAIL_IGNORE);
                    $s->setConfirmEmailError('is-spam:' . $spam_results[$s->getEmail()]);
                    $s->setStatus('invalid'); // @todo: make it a constant
                }
                $logger->info('Classifying {c} signatures as spam: {ids}', ['c' => count($ids), 'ids' => implode(',', $ids)]);
            });
        }

        if (count($signatures)) {
            $output->writeln(sprintf('Sending confirmation emails for %d signatures...', $count));
            $r = $this->confirmation_controller->processMultiple($signatures);
            $output->writeln(print_r($r, true));
        }

        return 0;
    }
}
