<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class DatabaseSanitizeCommand extends Command
{
    protected static $defaultName = 'db:clear';
    private $em;
    private $logger;

    public function __construct(LoggerInterface $logger, EntityManagerInterface $em)
    {
        $this->logger = $logger;
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Removes personal informations from the database')
            ->addOption('yes', 'y', InputOption::VALUE_NONE, 'Do not ask questions')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        // Make sure to get the user consent
        // (this is a dangerous command)
        $yesMode = $input->getOption('yes');
        if (!$yesMode) {
            $io->warning('This will erase all data and replace all entries with random data.');
            if (!$io->confirm('Do you want to erase all signatures and set random data?', false)) {
                $io->note('Action cancelled');
                return 1;
            }
        }

        // SQL statement to replace personal data with random infos
        $sql = <<<SQL
UPDATE signature SET first_name = SUBSTRING(MD5(RAND()) FROM 1 FOR 10),
                     last_name = SUBSTRING(MD5(RAND()) FROM 1 FOR 10),
                     email = CONCAT(SUBSTRING(MD5(RAND()) FROM 1 FOR 6), '@', SUBSTRING(MD5(RAND()) FROM 1 FOR 6), '-email.com'),
                     ip_address = CONCAT(ROUND(RAND() * 255), '.', ROUND(RAND() * 255), '.', ROUND(RAND() * 255), '.', ROUND(RAND() * 255))
;
SQL;

        // Execute the raw SQL
        $stmt = $this->em->getConnection()->prepare($sql);
        $stmt->execute();

        $io->success(sprintf('Updated %d rows', $stmt->rowCount()));

        return 0;
    }
}
