<?php

namespace App\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Serializer\Encoder\CsvEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Petition;
use App\Entity\Signature;

class SignaturesExportCommand extends Command
{
    protected static $defaultName = 'signatures:export';
    private $em;
    private $logger;

    public function __construct(LoggerInterface $logger, EntityManagerInterface $em)
    {
        $this->em = $em;
        $this->logger = $logger;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Exports a given petition\'s signatures as CSV.')
            ->addArgument('petitionId', InputArgument::REQUIRED, 'Petition identifier')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $petition = $this->em->getRepository(Petition::class)->find((int)($input->getArgument('petitionId')));
        if (!$petition) {
            $io = new SymfonyStyle($input, $output);
            $io->error(sprintf('Petition #%s does not exist', $petitionId));
            return 1;
        }

        $signatures = $this->em->getRepository(Signature::class)->findBy(
            ['petition' => $petition, 'status' => 'accepted'],
            ['id' => 'ASC']
        );

        // export signatures to a dict
        $rows = array_map(
            function ($signature) {
                return [
                    'id' => $signature->getId(),
                    'created' => $signature->getCreatedAt()->format('Y-m-d'),
                    'first_name' => $signature->getFirstName(),
                    'last_name' => $signature->getLastName(),
                    'email' => $signature->getEmail(),
                    'country' => $signature->getCountry(),
                    'postal_code' => $signature->getPostalCode(),
                    'city' => $signature->getCity(),
                    'newsletter' => (in_array(
                        $signature->getNewsletterStatus(),
                        [ 'todo', 'done' ]
                    ) ? 'true' : 'false'),
                ];
            },
            $signatures
        );

        $serializer = new Serializer([new ObjectNormalizer()], [new CsvEncoder()]);
        print($serializer->encode($rows, 'csv'));

        return 0;
    }
}
