<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Petition;
use App\Entity\Signature;
use App\Service\ConfirmationMessageFieldSetSelector;
use App\Controller\ConfirmationController;

class PetitionInfoCommand extends Command
{
    protected static $defaultName = 'petition:info';
    private $em;
    private $confirmation_controller;
    private $translator;
    protected $fieldsetSelector;

    public function __construct(
        EntityManagerInterface $em,
        TranslatorInterface $translator,
        ConfirmationController $confirmation_controller,
        ConfirmationMessageFieldSetSelector $fieldsetSelector
    ) {
        $this->em = $em;
        $this->translator = $translator;
        $this->confirmation_controller = $confirmation_controller;
        $this->fieldsetSelector = $fieldsetSelector;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Get infos about a petition')
            ->addArgument('petition-id', InputArgument::REQUIRED, 'Petition identifier')
            ->addArgument('signature-id', InputArgument::OPTIONAL, "Optional signature id used to generate a sample email's body.")
            ->addOption('lang', null, InputOption::VALUE_REQUIRED, "Override signature language, used to test email's body.")
            ->addOption('country', null, InputOption::VALUE_REQUIRED, "Override signature country, used to test email's body.")
            ->addOption('extra', null, InputOption::VALUE_REQUIRED, "Override signature extra, used to test email's body.")
            ->addOption('expr', null, InputOption::VALUE_REQUIRED, "Override petition template policy.");
        ;
    }

    public function makeSignature($petition, int $signature_id)
    {
        if ($petition) {
            if (is_int($petition)) {
                $petition = $this->em->find('App\Entity\Petition', $petition);
            } elseif (! ($petition instanceof Petition)) {
                throw new \RuntimeException('Invalid petition-id=$petition argument');
            }
        }

        $signature = null;
        if ($signature_id) {
            if (is_int($signature_id)) {
                $signature = $this->em->find('App\Entity\Signature', $signature_id);
            } elseif (! ($petition instanceof Signature)) {
                throw new \RuntimeException('Invalid signature-id=$signature_id argument');
            }
            if (! $petition) {
                $petition = $signature->getPetition();
            }
        }

        if (! $signature) {
            $signature = new class extends Signature
            {
                public $id;
                public function getId()
                {
                    return $this->id;
                }
                public function setId($id)
                {
                    $this->id = $id;
                }
            };
            $signature->setId('');
            $signature->setEmail('sample@localhost.local');
            $signature->setPetition($petition);
        }

        return $signature;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $petitionId = $input->getArgument('petition-id');
        $this->show($petitionId, $input, $output);

        return 0;
    }

    protected function show(int $petitionId, InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $expr = $input->getOption('expr');
        $lang = $input->getOption('lang');
        $country = $input->getOption('country');
        $extra = $input->getOption('extra');
        $petition = $this->em->getRepository(Petition::class)->find($petitionId);

        if (!$petition) {
            $io->error(sprintf('Petition #%s does not exist', $petitionId));
            return;
        }

        $io->title(sprintf('Infos about petition #%d', $petition->getId()));

        $io->text(sprintf('ID: %s', $petition->getId()));
        $io->text(sprintf('slug: %s', $petition->getSlug()));
        $io->text(sprintf('default title: %s', $petition->getTitle()));

        $io->section('Petition sync settings');
        $io->text(sprintf('mailchimpSource: %s', $petition->getMailchimpSourceOrSlug()));
        $io->text(sprintf('mailingListId: %s', $petition->getMailingListId()));
        $io->text(sprintf('groups: %s', implode(', ', $petition->getGroups())));


        $signature = $this->makeSignature($petition, (int)$input->getArgument('signature-id'));
        if ($lang) {
            $signature->setLanguage($lang);
        }
        if ($country) {
            $signature->setCountry($country);
        }
        if ($extra) {
            $signature->setExtra(json_decode($extra, true));
        }

        $fieldset = $this->fieldsetSelector->get($petition, $signature->getExtra()['variant'] ?? false);

        $io->section('Petition email settings');
        $io->text(sprintf('email title: %s', $fieldset->getTitle()));
        $io->text(sprintf('emailFromName: %s', $fieldset->getEmailFromName()));
        $io->text(sprintf('emailFromEmail: %s', $fieldset->getEmailFromEmail()));
        $io->text(sprintf('emailReplyTo: %s', $fieldset->getEmailReplyTo()));
        $io->text(sprintf('emailSubject: %s', $fieldset->getEmailSubject()));

        $io->section('Petition email content');
        $io->text(sprintf('subtitle: %s', $fieldset->getSubtitle()));
        $io->text(sprintf('formPageUrl: %s', $fieldset->getFormPageUrl()));
        $io->text(sprintf('shareTwitterUrl: %s', $fieldset->getShareTwitterUrl()));
        $io->text(sprintf('shareFacebookUrl: %s', $fieldset->getShareFacebookUrl()));

        if ($output->isVerbose()) {
            if ($expr) {
                $petition->setEmailTemplatePaths($expr);
            }

            $template = '';
            try {
                $message = $this->confirmation_controller->getConfirmationEmailInformation($signature, $template);
                if ($message === false) {
                    $io->text('Sending email is disabled for this case.');
                } else {
                    $from = implode(',', array_map(function ($data) {
                        return $data->toString();
                    }, $message->getFrom()));

                    $to = implode(',', array_map(function ($data) {
                        return $data->toString();
                    }, $message->getTo()));

                    $io->text(sprintf('Template: %s', $template));
                    $io->text(sprintf('From: %s', $from));
                    $io->text(sprintf('To: %s', $to));
                    $io->text(sprintf('Subject: %s', $message->getSubject()));
                    if ($output->isVeryVerbose()) {
                        $io->text(sprintf('Headers: %s', print_r($message->getHeaders()->toArray(), true)));
                        $io->text(sprintf('Body: %s', $message->getHtmlBody()));
                    }
                }
            } catch (\Exception $e) {
                $io->text(sprintf('Template: %s', $template));
                $io->text(sprintf('An exception arised while trying to build the email'));
                $io->text($e);
            }
        }

        $signature_repo = $this->em->getRepository(Signature::class);
        $io->section('Signatures');
        $io->text(sprintf('Accepted: %s', $signature_repo->getSignatureCount($petition)));
        $io->text(sprintf('Bounced: %s', $signature_repo->getSignatureCount($petition, 'bounced')));
        $io->text(sprintf('Revoked: %s', $signature_repo->getSignatureCount($petition, 'revoked')));
    }
}
