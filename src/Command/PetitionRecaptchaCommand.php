<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

use App\Entity\Petition;

class PetitionRecaptchaCommand extends Command
{
    protected static $defaultName = 'petition:recaptcha';
    private $em;
    private $params;

    public function __construct(EntityManagerInterface $em, ParameterBagInterface $params)
    {
        $this->em = $em;
        $this->params = $params;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Set/Unset recaptcha on a specific petition')
            ->addArgument('petitionId', InputArgument::REQUIRED, 'Petition identifier')
            ->addArgument('action', InputArgument::OPTIONAL, 'on|off')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $petitionId = $input->getArgument('petitionId');
        $action = $input->getArgument('action');
        $petition = $this->em->getRepository(Petition::class)->find($petitionId);

        if (!$petition) {
            $io->error(sprintf('Petition #%s does not exist', $petitionId));
            return 1;
        }

        $requirements = $petition->getRequirements();
        $status = in_array('recaptcha', $requirements);
        $global_status = $this->params->get('recaptcha_enabled');
        $secret = $this->params->get('recaptcha_secret');

        $do_on = in_array($action, ['1','on','yes','y','enable','set']);
        $do_off = in_array($action, ['0','off','no','n','disable','unset']);

        if (!$secret && ($global_status || $do_on || ($status && !$do_off))) {
            $io->warning('No reCaptcha secret key configured');
        }

        if (!$action || (!$do_on && !$do_off)) {
            $io->text($status ? 'on' : ($global_status ? 'off [global=on]' : 'off'));
            return 1;
        }

        if ($do_on && $status) {
            $io->text(sprintf("Recaptcha is already enabled on petition " . $petitionId));
            return 0;
        }

        if ($do_off && !$status) {
            $io->text(sprintf("Recaptcha is already disabled on petition " . $petitionId));
            return 0;
        }

        if ($do_on) {
            $requirements[] = 'recaptcha';
        } elseif ($do_off) {
            $requirements = array_filter($requirements, function ($e) { return $e != 'recaptcha'; });
        }

        $petition->setRequirements($requirements);
        $this->em->flush();
        $status = in_array('recaptcha', $petition->getRequirements());
        $io->text($status ? 'on' : 'off');

        return 0;
    }
}
