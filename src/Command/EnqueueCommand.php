<?php

/*
 * This file is part of Cygne.
 *
 * (c) Raphaël Droz <raphael@droz.eu>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Logger\ConsoleLogger;
use Doctrine\Common\Collections\Criteria;
use Doctrine\Common\Collections\Expr\CompositeExpression;
use Psr\Log\LoggerInterface;
use App\Entity\Signature;
use App\Repository\SignatureRepository;
use App\Repository\PetitionRepository;
use Cygne\CygneSyncBundle\Controller\SignatureQueueController;

class EnqueueCommand extends Command
{

    protected static $defaultName = 'cygne:sync-enqueue';

    const STATUS_ALLOWED = ['todo', 'ignore'];

    private $p_repository;
    private $s_repository;
    private $queueController;
    private $logger;

    public function __construct(
        LoggerInterface $logger,
        SignatureRepository $s_repository,
        PetitionRepository $p_repository,
        ?SignatureQueueController $queueController
    ) {
        $this->logger = $logger;
        $this->s_repository = $s_repository;
        $this->p_repository = $p_repository;
        $this->queueController = $queueController;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription("Enqueue signatures for later synchronization..")
             ->addOption('extra-parameters', null, InputOption::VALUE_REQUIRED, 'Arbitrary extra-parameter to pass to the synchronization driver in query-string format. Eg method=POST&status=subscribed', '')
             ->addOption('expr', null, InputOption::VALUE_REQUIRED, "Override petition synchronization policy.")
             ->addOption('limit', null, InputOption::VALUE_REQUIRED, 'Enqueue that many items. Value of 0 (default) enqueues ALL items having status `todo`')
             ->addOption('not-before', null, InputOption::VALUE_REQUIRED, 'Restrict item created during the last <time spec>. Eg 2d ago, 2 days ago, 3 minutes ago, ...')
             ->addOption('only', null, InputOption::VALUE_REQUIRED, 'Limit sync to these submissions only (ID or begining of email).')
             ->addOption('newsletter-status', null, InputOption::VALUE_REQUIRED, 'Limit to signatures having this newsletter status (default: "todo")', '')
             ->addOption('no-sns', null, InputOption::VALUE_NONE, 'Disregard whether thank-you email were delivered. Base the heuristic on whether they were successfully sent')
             ->addOption('petition', null, InputOption::VALUE_REQUIRED, 'Limit to these (comma-separated) petition ID(s).', '')
             ->addOption('show', null, InputOption::VALUE_NONE, 'Only shows synchronization status for these signatures.')
             ->addOption('dry-run', null, InputOption::VALUE_NONE, 'Show what would happen.')
             ->addOption('resync', null, InputOption::VALUE_NONE, 'Allow the creation new synchronizations for signatures already tried. Use with care.')
             ->setHelp('Enqueue signatures (whose newsletter_status is "todo") for later synchronization.')
            ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $limit = is_numeric($input->getOption('limit')) ? (int)$input->getOption('limit') : null;
        $petitionIds = array_filter(array_unique(array_filter(explode(',', $input->getOption('petition')), 'intval')));
        $newsletterStatuses = array_unique(array_filter(explode(',', $input->getOption('newsletter-status')), 'strlen'));
        $only = $input->getOption('only');
        $dry_run = $input->getOption('dry-run');
        $just_show = $input->getOption('show');
        $notBefore = $input->getOption('not-before');
        if ($notBefore) {
            $notBefore = new \DateTime($notBefore);
            if ($notBefore >= new \DateTime()) {
                throw new \RuntimeException($notBefore->format('Y-m-d-H-i-s') . ' is is the future. Check the value.');
            }
        }
        $allowDups = $input->getOption('resync');

        if (!$this->queueController) {
            throw new \RuntimeException('This command needs cygne/cygne-sync-bundle to work.');
        }

        // Value from Entity\Signature::$newsletterStatuses
        if ($newsletterStatuses) {
            if (count(array_intersect($newsletterStatuses, self::STATUS_ALLOWED)) != count($newsletterStatuses)) {
                throw new \RuntimeException(
                    sprintf(
                        'Invalid value for newsletter-status. Accepted: todo|ignore. Found: "%s"',
                        implode(',', array_diff($newsletterStatuses, self::STATUS_ALLOWED))
                    )
                );
            }
            if ($newsletterStatuses) {
                $output->writeln('<comment>Keep in mind that individual sync-drivers may ignore signatures'
                                 . ' whose `newsletter-status` = "ignore"</comment>');
            }
        }

        /**
         * Collect signatures in STATE_TODO and create corresponding record(s),
         * one per-destination, in the {synchronization} table.
         * Skip recreating a given synchronization record if such already exists in that table.
         *
         * Note: A signature is up for synchronization *if*:
         * 1. This has been requested (STATE_TODO)
         * 2. The thank you email has been sent
         * 3. The thank you email has been delivered (after a SNS confirmation was received)
         * (if email bounced, then status=bounced and confirmEmailStatus=error)
         */
        $criteria = Criteria::create()
                  ->andWhere(Criteria::expr()->eq('status', 'accepted'))
                  ->orderBy(['id' => Criteria::ASC]);

        /**
         * In case SNS is temporarily unavailable/broken, synchronization can be tweaked with the --no-sns options.
         * It would consider whether the thank-you email was simply sent (EMAIL_SENT)
         * rather than expecting delivery confirmation (Signature::EMAIL_DELIVERED)
         */
        $email_ok_statuses = [Signature::EMAIL_DELIVERED, Signature::EMAIL_IGNORE];
        if ($input->getOption('no-sns')) {
            $email_ok_statuses[] = Signature::EMAIL_SENT;
        }
        $criteria->andWhere(Criteria::expr()->in('confirmEmailStatus', $email_ok_statuses));

        /**
         * Whether newsletter/synchronization is desired
         */
        if ($newsletterStatuses) {
            $criteria->andwhere(Criteria::expr()->in('newsletterStatus', $newsletterStatuses));
        } else {
            $criteria->andwhere(Criteria::expr()->eq('newsletterStatus', 'todo'));
        }

        // Signature updated within the last 3 days (but any value older than last cron run is suitable)
        $dateCriterias = [Criteria::expr()->gt('updatedAt', new \DateTime('3 days ago'))];
        if ($notBefore) {
            $dateCriterias[] = Criteria::expr()->gt('createdAt', $notBefore);
        }

        $criteria->andwhere(new CompositeExpression(CompositeExpression::TYPE_OR, $dateCriterias));
        if ($only) {
            if (is_numeric($only)) {
                $criteria->andWhere(Criteria::expr()->eq('id', (int)$only));
            } else {
                $criteria->andWhere(Criteria::expr()->startsWith('email', $only));
            }
        }

        if (!$petitionIds && $limit) {
            $criteria->setMaxResults($limit);
        } elseif ($petitionIds) {
            $petition = $this->p_repository->findBy(['id' => $petitionIds]);
            $criteria->andWhere(Criteria::expr()->in('petition', $petition));
        }

        $signatures = $this->s_repository->matching($criteria);
        if ($petitionIds) {
            $signatures = $signatures->filter(function ($element) use ($petitionIds) {
                return in_array($element->getPetition()->getId(), $petitionIds);
            });

            $signatures = $signatures->slice(0, $limit);
        }

        if (is_array($signatures) ? !count($signatures) : !$signatures->count()) {
            $output->writeln('No signature to enqueue with these criterias.');
            return 0;
        }
        if (!is_array($signatures)) {
            $signatures = $signatures->slice(0);
        }

        $output->writeln(
            sprintf('Found %s pending signatures.', count($signatures)),
            OutputInterface::VERBOSITY_VERBOSE
        );

        $extra_params = [];
        parse_str($input->getOption('extra-parameters'), $extra_params);
        $expr = $input->getOption('expr');

        /**
         * The controller will check that synchronizations do not already exist within the
         * {synchronization} table.
         */
        if ($just_show) {
            $logger = new ConsoleLogger($output);
            foreach ($signatures as $s) {
                $msgs = $this->queueController->getStatus($s);
                foreach ($msgs as $msg) {
                    call_user_func_array([$logger, $msg[0]], array_slice($msg, 1));
                }
            }

            return 0;
        }

        if ($allowDups) {
            $this->queueController->setResyncAllowed($allowDups);
        }

        $this->queueController->setDryRun($dry_run);
        $synchronizations = $this->queueController->queueAll($signatures, $expr, $extra_params);
        $output->writeln(
            sprintf(
                implode(
                    PHP_EOL,
                    ['Signatures: %d candidates apt for queue.',
                     'Synchronizations: %d.']
                ),
                count($signatures),
                count($synchronizations)
            )
        );

        return 0;
    }
}
