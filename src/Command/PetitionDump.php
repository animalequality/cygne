<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Encoder\YamlEncoder;

use Doctrine\ORM\EntityManagerInterface;

use App\Entity\Petition;

class PetitionDump extends Command
{
    protected static $defaultName = 'petition:dump';
    private $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Dump petition objects to Yaml');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $petitions = $this->em->getRepository(Petition::class)->findAll();
        $normalizer = new ObjectNormalizer(null, new CamelCaseToSnakeCaseNameConverter());
        $serializer = new Serializer(
            [new DateTimeNormalizer(\DateTime::RFC3339), $normalizer],
            [new YamlEncoder(null, null, ['yaml_inline' => 2])]
        );
        print(
            $serializer->serialize(
                $petitions,
                'yaml',
                ['ignored_attributes' => ['signatures', 'mailchimpSourceOrSlug']]
            )
        );

        return 0;
    }
}
