<?php

namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;

use App\Service\Revokator;
use App\Entity\Petition;
use App\Entity\Signature;

class GetRevokeLinksCommand extends Command
{
    protected static $defaultName = 'getrevokelinks';
    private $revokator;
    private $em;

    public function __construct(Revokator $revokator, EntityManagerInterface $em)
    {
        $this->revokator = $revokator;
        $this->em = $em;
        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setDescription('Get revoke links')
            ->addArgument('email', InputArgument::REQUIRED, 'Signer \'s email address')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $email = $input->getArgument('email');
        $signatures = $this->em->getRepository(Signature::class)->findBy(['email' => $email]);
        if (!$signatures) {
            $io->error(sprintf('Email %s not found', $email));
            return 1;
        }

        $revokator = $this->revokator;
        $rows = array_map(function ($signature) use ($revokator) {
            $petition = $signature->getPetition();
            return [
                $petition->getId(),
                $signature->getId(),
                $signature->getCreatedAt()->format('Y-m-d'),
                $revokator->getRevokeUrl($petition, $signature),
            ];
        }, $signatures);
        $io->table(['Petition #', 'ID', 'Created', 'Revoke URL'], $rows);

        return 0;
    }
}
