<?php

namespace App\Service;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use App\Service\CleantalkSpamCheck;
use App\Service\CleantalkRequestSpamCheck;

class SpamChecker
{
    public $apiEnabled = false;
    public $offline_email_blacklist = [];

    // check-mode: soft would not classify as spam in case of a failure to retrieve results
    // (timeout, uninstalled library, ...)
    public const OFFLINE_CHECK = 1;
    public const ONLINE_CHECK  = 2;
    public const FULL_CHECK    = self::ONLINE_CHECK | self::OFFLINE_CHECK;
    public const SOFT_CHECK    = 4;
    public const HARD_CHECK    = 8;

    // values returned by verifyMultiple()
    public const NOT_SPAM      = 0;
    public const FOUND_OFFLINE = 1;
    public const FOUND_ONLINE  = 2;

    private $ct;
    private $auth_key;

    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $em,
        string $cleantalk_url,
        $cleantalk_auth_key,
        $offline_email_blacklist
    ) {
        if ($offline_email_blacklist) {
            $this->offline_email_blacklist = array_flip(
                array_filter(
                    explode("\n", $offline_email_blacklist),
                    fn($e) => strpos($e, '@')
                )
            );
        }

        $this->apiEnabled = class_exists('Cleantalk\CleantalkAntispam') && $cleantalk_auth_key;
        if ($this->apiEnabled) {
            $this->ct = new CleantalkSpamCheck();
            if (! preg_match("@^https?://@", $cleantalk_url)) {
                // Cleantalk::sendRequest() is delicate
                $cleantalk_url = "https://$cleantalk_url";
            }
            $this->ct->server_url = $cleantalk_url;
            $this->auth_key = $cleantalk_auth_key;
        }
    }

    public function isSpamOffline(string $email)
    {
        return $email && isset($this->offline_email_blacklist[$email]);
    }


    public function isSpam(array $ips = [], array $emails = [])
    {
        if (!$this->apiEnabled) {
            return null;
        }

        $ct_request = new CleantalkRequestSpamCheck(
            ['data' => implode(',', array_merge($ips, $emails)),
             'auth_key' => $this->auth_key]
        );
        return $this->ct->isSpam($ct_request);
    }

    /**
     * @return array Associative array: email as key, int as whether an entry is a spam: 0=not spam, 1=spam-offline, 2=spam-online
     */
    public function verifyMultiple(array $to_check, int $mode, $logger = null): array
    {
        if (!count($to_check)) {
            return [];
        }

        $to_check = array_unique($to_check);
        if ($logger) {
            $logger->info(
                '{d} email addresses to check against local spam db of {c} items',
                ['d' => count($to_check), 'c' => count($this->offline_email_blacklist)]
            );
        }

        $is_spam_results = array_fill_keys($to_check, self::NOT_SPAM);

        if ($mode & self::OFFLINE_CHECK) {
            array_walk($to_check, function ($e) use (&$is_spam_results) {
                $is_spam_results[$e] = $this->isSpamOffline($e) ? self::FOUND_OFFLINE : self::NOT_SPAM;
            });
            $to_check = array_filter($to_check, fn($e) => !$is_spam_results[$e]);
        }

        if ($mode & self::ONLINE_CHECK) {
            $ret = $this->isSpam($to_check);
            if ($ret === null) {
                if ($mode & self::HARD_CHECK) {
                    throw \Exception("Online spam-checking unavailable but hard-mode requested: Aborting");
                } else {
                    return $is_spam_results;
                }
            }

            if ($logger) {
                $logger->info('{d} email addresses to check against online spam db', ['d' => count($to_check)]);
            }

            $is_spam_results = array_merge(
                $is_spam_results,
                array_map(fn ($e) => $e['spam_rate'] > 0 ? self::FOUND_ONLINE : self::NOT_SPAM, $ret['data'] ?: [])
            );
        }

        return $is_spam_results;
    }
}
