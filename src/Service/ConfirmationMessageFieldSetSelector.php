<?php

namespace App\Service;

use App\Entity\Petition;
use App\Entity\EmailMessageFieldSet;
use App\Repository\YamlPetitionRepository;

use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ConfirmationMessageFieldSetSelector
{

    /**
     * YamlPetitionRepository
     */
    private $yamlRepository = null;
    private $validator = null;
    private $serializer = null;
    private $logger;

    public function __construct(
        YamlPetitionRepository $yamlRepository,
        ValidatorInterface $validator,
        LoggerInterface $logger
    ) {
        $this->yamlRepository = $yamlRepository;
        $this->validator = $validator;
        $this->logger = $logger;

        $objectNormalizer = new ObjectNormalizer(
            null,
            new CamelCaseToSnakeCaseNameConverter(),
            null,
            new ReflectionExtractor()
        );
        $this->serializer = new Serializer([new ArrayDenormalizer(), $objectNormalizer]);
    }

    // public function get(Petition $p, int|string $variantId = null): EmailMessageFieldSet
    public function get(Petition $p, $variantId = null)
    {
        $petition = $this->yamlRepository->find($p->getId());
        return $this->getVariant($petition, $variantId);
    }

    // public function getVariant(Petition $p, int|string $variantId = null): EmailMessageFieldSet
    public function getVariant(Petition $p, $variantId = null)
    {
        $msgs = $p->getMessages();
        $defaults = $msgs['defaults'] ?? $msgs['default'] ?? $msgs['_'] ?? [];

        $msg = $defaults;
        if ($variantId) {
            if (!empty($msgs[$variantId])) {
                $msg = array_merge($defaults, $msgs[$variantId] ?? []);
            } else {
                $this->logger->warning('Requested a non-existing variant "{f}" on petition {p}',
                                       ['f' => $variantId, 'p' => $p->getId()]);
            }
        }

        $message = $this->serializer->denormalize($msg, EmailMessageFieldSet::class, null, []);
        $errors = $this->validator->validate($message);
        if (count($errors) > 0) {
            $str = "Petition {$p->getId()} variant \"{$variantId}\" loading error" . (string)$errors;
            $this->logger->warning($str);
            throw new \Exception($str);
        }
        return $message;
    }
}
