<?php

namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use Symfony\Component\ExpressionLanguage\SyntaxError;
use Symfony\Component\Mailer\Header\TagHeader;
use Symfony\Component\Mailer\Header\MetadataHeader;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\Mime\Address;
use Psr\Log\LoggerInterface;
use Twig\Error\LoaderError;
use Twig\Environment;
use App\Service\ConfirmationMessageFieldSetSelector;
use App\Service\Revokator;
use App\Entity\Petition;
use App\Entity\Signature;

/*
 * Examples of expressions supported:
 * See https://symfony.com/doc/current/components/expression_language/syntax.html
 *
 * @es/confirmation1.html.twig      A namespaced template file.
 * ['@' ~ lower(country), '@es']    First try namespace corresponding to signature's country, then fallback on @es Twig namespace.
 * country == 'UK' ? false : '@us'  Use @us namespace, except for UK signatoree who won't receive confirmation email (bad). 
 * lower(petition.lang) in ['mx','es'] ? '@es' : '@us'
 *
 */
class ConfirmationMessageBuilder
{
    protected $revokator;
    protected $twig;
    protected $original_twig_paths;
    protected $params;
    protected $translator;
    protected $fieldsetSelector;
    protected $logger;

    /**
     * ToDo: we should provide a default/neutral template
     */
    const DEFAULT_TEMPLATE_NAME = 'confirmation.html.twig';

    const TPL_IS_FILE = 1;
    const TPL_IS_DIR  = 1 << 1;
    const TPL_IS_NS   = 1 << 2;
    const TPL_IS_REL_PATH = 1 << 3;

    public function __construct(
        Environment $twig,
        Revokator $revokator,
        ContainerBagInterface $params,
        TranslatorInterface $translator,
        ConfirmationMessageFieldSetSelector $fieldsetSelector,
        LoggerInterface $logger
    ) {
        $this->twig = $twig;
        $this->revokator = $revokator;
        $this->params = $params;
        $this->translator = $translator;
        $this->original_twig_paths = $twig->getLoader()->getPaths();
        $this->logger = $logger;
        $this->fieldsetSelector = $fieldsetSelector;
    }

    /**
     * Get a template's path for a given petition,
     * using email_template_paths field
     *
     * If no email is expected to be sent for a given signature/petition, then
     * false is returned.
     * To avoid any confusion, null is not expected to be returned under any condition.
     *
     */
    public function getTemplatePath(Signature $signature) // : string|false
    {
        $petition = $signature->getPetition();
        $expression = $petition->getEmailTemplatePaths();

        try {
            $fieldset = $this->fieldsetSelector->get($petition, $signature->getExtra()['variant'] ?? false);
            if ($variant_expression = $fieldset->getEmailTemplatePaths()) {
                $expression = $variant_expression;
            }
        } catch (\Exception $e) {}

        $paths = $this->getTemplatesByExpression(
            $expression,
            ['p' => $petition,
             's' => $signature,
             'wants_nl' => $signature->getNewsletterStatus() !== 'ignore',
             'country' => $signature->getCountry()]
        );

        if ($paths === false) {
            return false;
        }
        if (!$paths) {
            throw new LoaderError(
                sprintf(
                    '[confirmation-email] No path from petition expression (id=%s, expr=%s)',
                    $petition->getId(),
                    $expression
                )
            );
        }

        $template = $this->getTemplate($paths);
        if (!$template) {
            throw new LoaderError(
                sprintf(
                    '[confirmation-email] No usable email template (id=%s, expr=%s)',
                    $signature->getId(),
                    $expression
                )
            );
        }

        return $template;
    }

    /**
     * Get a template's HTML for a given petition,
     * using email_template_paths field
     *
     * If not email is expected to be sent for a given signature/petition, then
     * false is returned.
     * To avoid any confusion, null is not expected to be returned under any condition.
     *
     */
    public function getHtml(Signature $signature, string $template)
    {
        $petition = $signature->getPetition();
        $fieldset = $this->fieldsetSelector->get($petition, $signature->getExtra()['variant'] ?? false);

        if ($signature->getLanguages()) {
            // $this->translator->setLocale($language);
        }

        $html = $this->twig->render($template, [
            'petition' => $petition,
            'signature' => $signature,
            'petition_title' => $fieldset->getTitle() ?: $petition->getTitle(),
            'petition_groups' => $petition->getGroups(),
            'fields' => $fieldset,
            'extra_fields' => $fieldset->getExtra(),
            'petition_page' => $fieldset->getFormPageUrl(),
            'twitter_url' => $fieldset->getShareTwitterUrl(),
            'revokeUrl' => $this->revokator->getRevokeUrl($petition, $signature),
        ]);

        // Reset paths to ensure consistent state when sending multiple confirmations
        // of multiple petitions in a row.
        $this->twig->getLoader()->setPaths($this->original_twig_paths);

        if (!$html) {
            // If twig->render() succeeded (no 'Unable to find template' exception), but template is empty,
            // let's fail to avoid sending empty email.
            throw new LoaderError(
                sprintf(
                    '[confirmation-email] HTML seems empty for petition petition-id=%s, signature-id=%s',
                    $petition->getId(),
                    $signature->getId()
                )
            );
        }

        return $html;
    }

    public function getEmailMessage(Signature $signature, string $html): TemplatedEmail
    {
        $petition = $signature->getPetition();
        $fieldset = $this->fieldsetSelector->get($petition);

        $email = (new TemplatedEmail())
               ->from(
                   new Address($fieldset->getEmailFromEmail(), $fieldset->getEmailFromName())
               )
               ->to(
                   new Address(
                       $signature->getEmail(),
                       $signature->getFirstName() . ' ' . $signature->getLastName()
                   )
               )
               ->replyTo($fieldset->getEmailReplyTo())
               ->subject($fieldset->getEmailSubject() ?: $this->translator->trans('Your Petition Signature'))
               ->html($html);

        // Mandrill
        $email->getHeaders()->add(new TagHeader('cygne'));
        $email->getHeaders()->add(new MetadataHeader('signature_id', $signature->getId()));
        // AWS (Note: This would only work with the SMTP API but not with the native API)
        $email->getHeaders()->addTextHeader('X-SES-CONFIGURATION-SET', 'data-event-logging');

        return $email;
            /*
            // ToDo: Use htmlTemplate()
               ->context([
                   'petition' => $petition,
                   'signature' => $signature,
                   'revokeUrl' => $this->revokator->getRevokeUrl($petition, $signature),
               ]);
            */
    }

    private function templateType(string $p)
    {
        $ret = 0;
        if (! $p) {
            return 0;
        }
        if (strpos($p, '@') === 0) {
            $ret |= self::TPL_IS_NS;
        } elseif (preg_match('!^[/%@]!', $p)) {
            $ret |= self::TPL_IS_REL_PATH;
        }
        if (strpos($p, '.html.twig') === false) {
            $ret |= self::TPL_IS_DIR;
        } else {
            $ret |= self::TPL_IS_FILE;
        }

        return $ret;
    }

    /**
     * Expand an expression representing a (possibly list of) template(s) to use for
     * the email confirmation of this petition.
     *
     * This will first try to proceed using ExpressionLanguage, where signature (s), petition (p) and country are
     * available variable. This expression can return one (or a list of) template or namespace.
     * Values string starting by a '@' are considered namespaces.
     *
     * If it fails to parse as an expression, then it's considered a comma-separated list of values.
     * If a list is returned, first match wins.
     *
     * @return array|bool The template(s) to be used
     *               false indicate no template must be used
     */
    private function getTemplatesByExpression(string $expression, array $expr_params) // : array|bool
    {
        $paths = $template = null;
        $expressionLanguage = new ExpressionLanguage();
        $expressionLanguage->register('lower', function ($str) {
            return sprintf('(is_string(%1$s) ? strtolower(%1$s) : %1$s)', $str);
        }, function ($arguments, $str) {
            if (!is_string($str)) {
                return $str;
            }
            return strtolower($str);
        });

        try {
            $paths = $expressionLanguage->evaluate(
                $expression,
                $expr_params + ['default' => self::DEFAULT_TEMPLATE_NAME]
            );
            if (!$paths) {
                return false; // email confirmation disabled
            }
            if (!is_array($paths)) {
                $paths = [$paths];
            }
        } catch (SyntaxError $e) {
            if ($expression === 'false') {
                return false;
            }
            $paths = $expression ? explode(',', $expression) : [];
        }

        return $paths;
    }

    /**
     * Choose a template from a list passed as an array.
     *
     * $paths array
     */
    private function getTemplate(array $paths): string
    {
        foreach ($paths as $name) {
            if ($name === false) {
                // If an array of templates/namespace was returned, then false just means
                // the same as being the last template in the list.
                // There is no point in doing this.
                return false;
            }
            if (!is_string($name)) {
                continue;
            }

            $template = $this->checkTemplate($name);
            if ($template && is_string($template) || $template === false) {
                return $template;
            }
        }

        $namespaces = $this->twig->getLoader()->getNamespaces();
        $this->logger->warning(
            'No template found, using default "{d}". paths = [{paths}], Namespace = {ns}',
            ['paths' => implode(',', $paths),
             'ns' => implode(',', $namespaces),
             'd' => self::DEFAULT_TEMPLATE_NAME]
        );

        return self::DEFAULT_TEMPLATE_NAME;
    }

    /**
     * Look for an existing and usable template from what has been provided for this petition email_template_paths.
     * @return string|false The template to be used
     *                      false indicate no template must be usee, null indicate no template could be found
     *
     */
    public function checkTemplate($name)
    {
        $loader = $this->twig->getLoader();
        $twig_paths = $loader->getPaths();
        $namespaces = $loader->getNamespaces();

        $t = $this->templateType($name);
        // Namespace template filename
        if (($t & ~self::TPL_IS_NS & self::TPL_IS_FILE) && $loader->exists($name)) {
            return $name;
        }

        // Namespace only
        if (($t & self::TPL_IS_NS & ~self::TPL_IS_FILE)) {
            if (! in_array(ltrim($name, '@'), $namespaces, true)) {
                return null; // skip to next one
            }

            // Otherwise append default template filename
            $template = rtrim($name, '/') . '/' . self::DEFAULT_TEMPLATE_NAME;
            if ($loader->exists($template)) {
                return $template;
            }
        }

        // Non-Namespaced template filename
        if (($t & ~self::TPL_IS_NS & self::TPL_IS_FILE)) {
            // assume default location list
            if ($loader->exists($name)) {
                return $name;
            } else {
                foreach ($namespaces as $ns) {
                    $ns_template = "@" . $ns . '/' . $name;
                    if ($loader->exists($ns_template)) {
                        return $ns_template;
                    }
                }
            }
        }

        if (($t & self::TPL_IS_DIR & ~self::TPL_IS_FILE)) {
            $dir = $name;
            if (! in_array($dir, $twig_paths)) {
                try {
                    if (strpos($dir, '%') !== false) {
                        $dir = $this->params->resolveString($dir);
                    }
                    // $new_path = '%kernel.project_dir%/' . $new_path;
                    /**
                     * Append template paths to the Twig service and store old ones in $old_paths.
                     * The result will be stored in the given Signature object.
                     */
                    $loader->prependPath($dir);
                } catch (\Twig\Error\LoaderError $e) {
                }
                return null; // skip
            }
        }
    }
}
