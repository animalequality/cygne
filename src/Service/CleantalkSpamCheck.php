<?php

namespace App\Service;

use Cleantalk\Cleantalk;
use Cleantalk\CleantalkHelper;
use Cleantalk\CleantalkRequest;

class CleantalkSpamCheck extends Cleantalk
{
    public $ssl_on = true;
    private $server_timeout = 15;
    private $data;

    private function createCheckMsg(CleantalkRequest $request)
    {
        $request->method_name = 'spam_check';

        // Removing non UTF8 characters from request, because non UTF8 or malformed characters break json_encode().
        foreach ($request as $param => $value) {
            if (is_array($request->$param)) {
                $request->$param = CleantalkHelper::removeNonUTF8FromArray($value);
            }
            if (is_string($request->$param) || is_int($request->$param)) {
                $request->$param = CleantalkHelper::removeNonUTF8FromString($value);
            }
        }
        return $request;
    }

    public function isSpam(CleantalkRequest $request)
    {
        $filtered_request = $this->createCheckMsg($request);
        $this->data       = $filtered_request->data;

        return $this->httpBatchRequest($filtered_request);
    }

    /**
     * Send plain request to servers
     *
     * @param $data
     * @param $url
     * @param int $server_timeout
     *
     * @return boolean|array
     */
    private function sendPlainRequest($data, $url, $server_timeout = 3)
    {
        $data = (array)json_decode(json_encode($data), true);
        $data = array_filter($data, fn($e) => !is_null($e));

        if (isset($this->api_version)) {
            $url = $url . $this->api_version;
        }

        $result = false;
        $curl_error = null;

        $post = $data['data'];
        unset($data['data']);
        $get = http_build_query(array_filter($data));
        $url = $url . '?' . $get;

        if (function_exists('curl_init')) {
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_TIMEOUT, $server_timeout);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, ['data' => $post]);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // receive server response ...
            curl_setopt($ch, CURLOPT_HTTPHEADER, ['Expect:']); // resolve 'Expect: 100-continue' issue
            curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0); // see http://stackoverflow.com/a/23322368

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); // Disabling CA cert verivication and
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);     // Disabling common name verification

            if ($this->ssl_path != '') {
                curl_setopt($ch, CURLOPT_CAINFO, $this->ssl_path);
            }

            $result = curl_exec($ch);
            $curl_error = curl_error($ch);
            curl_close($ch);
        }

        if (!$result) {
            $allow_url_fopen = ini_get('allow_url_fopen');
            if (function_exists('file_get_contents') && (int)$allow_url_fopen) {
                $opts = ['http' => [
                    'method'  => 'POST',
                    'header'  => "Content-Type: text/html\r\n",
                    'content' => $post,
                    'timeout' => $server_timeout
                ]];

                $context = stream_context_create($opts);
                $result = @file_get_contents($url, false, $context);
            }
        }

        if (!$result || !CleantalkHelper::is_json($result)) {
            $response = null;
            $response['errno'] = 1;
            if ($curl_error) {
                $response['errstr'] = sprintf("CURL error: '%s'", $curl_error);
            } else {
                $response['errstr'] = 'No CURL support compiled in';
            }
            $response['errstr'] .= ' or disabled allow_url_fopen in php.ini.';
            return $response;
        }

        $response = json_decode($result, true);
        if ($result !== false && is_array($response)) {
            $response += ['errno' => 0, 'errstr' => null];
            return $response;
        }

        return ['errno' => 1, 'errstr' => 'Unknown response from ' . $url . '.' . ' ' . $result];
    }

    // Upstream reluctant to change httpRequest() visibility
    // https://github.com/CleanTalk/php-antispam/issues/40#issuecomment-2448136315
    protected function httpBatchRequest($msg)
    {
        $response = $this->sendPlainRequest($msg, $this->server_url, $this->server_timeout);
        return $response + ['sender_ip' => $this->sender_ip, 'sender_email' => $this->sender_email];
    }
}
