<?php

namespace App\Service;

use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

use App\Entity\Petition;
use App\Entity\Signature;

class Revokator {
    private $router;
    private $secret;

    public function __construct(Router $router, string $secret) {
        $this->router = $router;
        $this->secret = $secret;
    }

    public function getRevokeUrl(Petition $petition, Signature $signature): string
    {
        return $this->router->generate(
            'signature_revoke',
            [
                'email' => $signature->getEmail(),
                'petitionSlug' => $petition->getSlug(),
                'token' => $this->getRevokeToken($petition, $signature),
            ],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
    }

    protected function getRevokeToken(Petition $petition, Signature $signature): string
    {
        $petitionId = $petition->getId();
        $email = $signature->getEmail();
        return $this->getRevokeTokenSign($petitionId, $email);
    }

    public function getRevokeTokenSign(int $petitionId, string $email): string
    {
        $stringToSign = sprintf("%s\t%s", $petitionId, $email);
        return hash_hmac('sha256', $stringToSign, $this->secret, false);
    }
}
