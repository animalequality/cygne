<?php

namespace App\Controller;

use App\Entity\Petition;
use App\Entity\Signature;
use Doctrine\Persistence\ManagerRegistry;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class PetitionController extends AbstractController
{
    private $validator = null;
    private $doctrine = null;

    public function __construct(ValidatorInterface $validator, ManagerRegistry $doctrine)
    {
        $this->validator = $validator;
        $this->doctrine = $doctrine;
    }

    /**
     * Returns information about a petition.
     * This is a web/HTML endpoint.
     *
     * @Route("/petitions/{slug}", name="petition")
     *
     * @param string $slug Petition slug
     * @return Response
     */
    public function petition(string $slug, TranslatorInterface $translator, Request $request): Response
    {
        $petition = $this->doctrine->getRepository(Petition::class)->findOneBy(['slug' => $slug]);
        if (!$petition) {
            throw new NotFoundHttpException($translator->trans('Petition does not exist'));
        }
        $signatureCount = $this->doctrine->getRepository(Signature::class)->getSignatureCount($petition);
        $translator->setLocale($request->getPreferredLanguage($this->getParameter('supported_language')));
        $response = $this->render('petition.html.twig', array(
          'petition' => $petition,
          'signatureCount' => $signatureCount,
        ));
        $response->setSharedMaxAge(60);
        return $response;
    }

    /**
     * Returns information about a petition.
     * This is a JSON API endpoint.
     *
     * @Route(
     *     name="api_petition_item",
     *     path="/api/petitions/{id<[1-9]\d*>}",
     *     methods={"GET"}
     * )
     *
     * @param string $id Petition identifier
     * @return JsonResponse
     */
    public function apiPetition(string $id, TranslatorInterface $translator): JsonResponse
    {
        $petition = $this->doctrine->getRepository(Petition::class)->find($id);
        if (!$petition) {
            throw new NotFoundHttpException($translator->trans('Petition does not exist'));
        }
        $signatureCount = $this->doctrine->getRepository(Signature::class)->getSignatureCount($petition);
        $requirements = $petition->getRequirements();
        if ($this->getParameter('recaptcha_enabled') && !in_array('recaptcha',  $requirements)) {
            $requirements[] = 'recaptcha';
        }

        $response =  new JsonResponse([
            'id' => $petition->getId(),
            'slug' => $petition->getSlug(),
            'title' => $petition->getTitle(),
            'created' => $petition->getCreatedAt()->format(\DateTime::ISO8601),
            'signatureCount' => $signatureCount,
            'requirements' => $requirements,
        ]);
        $response->setSharedMaxAge(60);
        return $response;
    }

    /**
     * Returns the list of availabe petitions.
     * This is a JSON API endpoint.
     *
     * @Route(
     *     name="api_petition_list",
     *     path="/api/petitions",
     *     methods={"GET"}
     * )
     * @IsGranted("ROLE_CHECK", message="Access denied")
     *
     * @return JsonResponse
     */
    public function apiPetitionList(Request $request): JsonResponse
    {
        $petitions = $this->doctrine->getRepository(Petition::class)->findAll();
        $data = array_map(function ($petition) use ($request) {
            return
                [
                    'id' => $petition->getId(),
                    'slug' => $petition->getSlug(),
                    'title' => $petition->getTitle(),
                    'created' => $petition->getCreatedAt()->format(\DateTime::ISO8601),
                ]
                + (
                    $request->query->get('count')
                    ? ['signatureCount' => $this->doctrine->getRepository(Signature::class)->getSignatureCount($petition)]
                    : []
                );
        }, $petitions);

        $response = new JsonResponse($data);
        $response->setSharedMaxAge(60);
        return $response;
    }

    /**
     * Creates a new petition object
     *
     * @param string $data Petition parameters
     * @return Petition
     */
    public function createPetition(array $data): Petition
    {
        $em = $this->doctrine->getManager();

        // create a petition object
        $petition = new Petition();
        $petition->setTitle($data['title']);
        $petition->setSlug($data['slug']);
        $petition->setMailchimpSource($data['mailchimpSource']);
        $petition->setMailingListId($data['mailingListId']);
        $petition->setGroups($data['groups']);

        $errors = $this->validator->validate($petition);
        if (count($errors) > 0) {
            throw new \Exception((string)$errors);
        }

        // persist
        $em->persist($petition);
        $em->flush();

        return $petition;
    }
}
