<?php

namespace App\Controller;

use App\Entity\Petition;
use App\Entity\Signature;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class GraphController extends AbstractController
{

    public const COMMAND_LINE = "/usr/bin/gnuplot -e \"filename='%s';w=%d;h=%d\" \"%s\" 2>&1";

    public const MIN_WIDTH = 120;
    public const MIN_HEIGHT = 100;

    public const DEFAULT_WIDTH = 2200;
    public const DEFAULT_HEIGHT = 2000;

    public const MAX_WIDTH = 4200;
    public const MAX_HEIGHT = 4000;

    /**
     * $skipLowCount int Ignore petition having less than X signature
     */
    public $skipLowCount = 10;

    /**
     * $cache int Duration between CSV/PNG file regeneration in number of seconds (or false to disable)
     */
    public $cache = 3600 * 4 + 300;

    public $plotterFilename = false;
    public $outputDirectory = false;

    private $em;
    private $logger;

    public function __construct(
        EntityManagerInterface $em,
        LoggerInterface $logger,
        string $plotterFilename,
        string $outputDirectory
    ) {
        $this->em = $em;
        $this->logger = $logger;
        $this->plotterFilename = $plotterFilename;
        $this->outputDirectory = $outputDirectory;
    }

    public function setPlotterFilename($plotterFilename)
    {
        if (!file_exists($plotterFilename)) {
            throw new RuntimeException(sprintf('plot file %s not found', $plotterFilename));
        }

        $this->plotterFilename = $plotterFilename;
    }

    public function setOutputDirectory($outputDirectory)
    {
        $this->outputDirectory = $outputDirectory;
    }

    /**
     * Create graph files for a petition ID
     * This is a JSON API endpoint.
     *
     * @Route(
     *     name="graph_petition_by_slug",
     *     path="/api/graph/{id<[a-zA-Z0-9_-]+(,[a-zA-Z0-9_-]+)*>}",
     *     defaults={"width": 2200, "height": 2000},
     *     methods={"GET"},
     * )
     * @IsGranted("ROLE_CHECK", message="Access denied")
     *
     * @param int|string $id The petition ID or slug
     * @return Response
     */
    public function graphPetition(Request $request, string $id, int $width, int $height): Response
    {
        $petitions = $this->getPetitions(explode(',', $id));
        $width = max(min($width, self::MAX_WIDTH), self::MIN_WIDTH);
        $height = max(min($height, self::MAX_HEIGHT), self::MIN_HEIGHT);
        $filenames = $this->generateGraphs($petitions, $width, $height);

        $output = ["<!DOCTYPE html>"];
        foreach ($filenames as $filename) {
            $output[] = sprintf(
                '<img src="data:image/png;base64,%s" alt="%s" />',
                base64_encode(file_get_contents($filename)),
                basename($filename)
            );
        }

        $response = new Response(implode("\n", $output));
        $response->headers->set('Content-Type', 'text/html');
        return $response;
    }

    public function getPetitions($selectors)
    {
        $prepo = $this->em->getRepository(Petition::class);
        if (in_array('all', $selectors)) {
            return $prepo->findAll();
        }

        $pids = array_unique(array_filter($selectors, 'is_numeric'));
        $pslugs = array_unique(
            array_filter(
                $selectors,
                function ($e) {
                    return !is_numeric($e);
                }
            )
        );

        $criteria = new Criteria();
        if ($pids) {
            $criteria->orWhere($criteria->expr()->in('id', $pids));
        }
        if ($pslugs) {
            $criteria->orWhere($criteria->expr()->in('slug', $pslugs));
        }
        return $prepo->matching($criteria)->getIterator();
    }

    /**
     * @return string The list of filenames generated.
     */
    public function generateGraphs($petitions, int $width, int $height): array
    {
        $filenames = [];
        foreach ($petitions as $petition) {
            $id = $petition->getId();
            $slug = $petition->getSlug();
            $csvFilename = sprintf("%s/petition-%s.txt", $this->outputDirectory, $petition->getSlug());
            // See config/Plotter.gp
            $graphFilename = sprintf("%s-%dx%d.png", $csvFilename, $width, $height);

            // Note: $petition->getSignatures()->count() consumes 30MB of memory
            $count = $this->em
                   ->getRepository(Signature::class)
                   ->createQueryBuilder('s')
                   ->select('count(1)')
                   ->where('s.petition = :pid')
                   ->andWhere('s.status = :status')
                   ->setParameter('pid', $id)
                   ->setParameter('status', 'accepted')
                   ->getQuery()
                   ->getSingleScalarResult();

            if (!$this->cache || !file_exists($csvFilename) || time() - filemtime($csvFilename) > $this->cache) {
                if ($this->skipLowCount > 0 && $count < $this->skipLowCount) {
                        continue;
                }

                $this->logger->info(
                    'Petition {id}:{slug} has {count} signatures',
                    ['id' => $id, 'slug' => $slug, 'count' => $count]
                );

                $ok = $this->generateCsvFile($id, $csvFilename);
                if (! $ok) {
                    continue;
                }

                $this->logger->info(
                    'Stored {count} lines into {file}.',
                    ['count' => $ok, 'file' => $csvFilename]
                );
            }

            if (!$this->cache || !file_exists($graphFilename) || time() - filemtime($graphFilename) > $this->cache) {
                $ret = $this->graphFile($csvFilename, $width, $height);
            }

            $filenames[] = $graphFilename;
        }

        return $filenames;
    }

    public function generateCsvFile(int $id, string $csvFilename): int
    {
        $sql = "SELECT DATE_FORMAT(MAX(created_at), '%Y-%m-%dT%H:%i:%s') as d, COUNT(id) as c"
             . " FROM signature WHERE petition_id = :petition"
             . " GROUP BY DATE_FORMAT(created_at, '%Y-%m-%d %H:%i')";
        $stmt = $this->em->getConnection()->prepare($sql);
        $q = $stmt->executeQuery(['petition' => $id]);

        $results = [];
        foreach ($q->fetchAllAssociative() as $item) {
            $results[$item['d']] = $item['c'];
        }

        return $results ? $this->writeFile($csvFilename, $results) : 0;
    }

    public function writeFile($filename, $results): int
    {
        if (!is_dir(dirname($filename))) {
            mkdir(dirname($filename), 0750, true);
        }

        $fp = fopen($filename, 'w');
        fputcsv($fp, ['date', 'count'], "\t");
        $counter = 0;
        foreach ($results as $k => $v) {
            fputcsv($fp, [$k, $v], "\t");
            $counter++;
        }
        fclose($fp);

        return $counter;
    }

    public function graphFile($filename, $width, $height): bool
    {
        $cmd = sprintf(self::COMMAND_LINE, $filename, $width, $height, $this->plotterFilename);

        if (in_array('popen', explode(',', ini_get('disable_functions')))) {
            $this->logger->warning("popen is disabled, can't exec {cmd}", ['cmd' => $cmd]);
            return false;
        }

        $output = popen($cmd, 'r');
        if (!$output) {
            return false;
        }
        $this->logger->info('{cmd} returned {output}', ['cmd' => $cmd, 'output' => fread($output, 5000)]);
        return pclose($output) === 0;
    }
}
