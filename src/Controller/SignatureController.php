<?php

namespace App\Controller;

use App\Encoder\SignatureObject;
use App\Entity\Petition;
use App\Entity\Signature;
use App\Exception\StatusMismatchException;
use App\Repository\ObfuscatedSignatureRepository;
use App\Repository\PetitionRepository;
use App\Repository\SignatureRepository;
use App\Repository\YamlPetitionRepository;
use App\Service\Revokator;
use App\Service\ConfirmationMessageFieldSetSelector;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\DBALException;
use Doctrine\Persistence\ManagerRegistry;
use Noxlogic\RateLimitBundle\Annotation\RateLimit;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Process\Process;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Serializer\Normalizer\AbstractNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Cygne\CygneSyncBundle\Command\BaseWebtrackCommand;

class SignatureController extends AbstractController
{
    const EXTRA_PAYLOAD_LIMIT = 1024 * 4;

    /**
     * When computing a data-diff between two signatures, these fields are omitted.
     */
    public const SIGNATURE_COMPARISON_OMITTED_FIELDS = ['petition', 'confirmEmailStatus', 'newsletterStatus', 'ipAddress', 'groupSequence'];

    /**
     * If an identical signature is resubmitted and the previous attempt was
     * old than this, then a resynchronization will be triggered.
     * Sample value: "now - 3 days"
     */
    const RESYNC_NEW_SUBMISSION_AFTER_PERIOD = 'now - 3 days';

    private $redirectError = null;
    private $doctrine = null;
    private $signatureRepository = null;
    private $petitionRepository = null;
    private $fieldsetSelector;
    private $obfuscatedSignatureRepository = null;
    private $projectDir = null;
    private YamlPetitionRepository $yamlPetitionRepository;
    private LoggerInterface $logger;
    private array $metaConversionApiConfig;

    public function __construct(
        ManagerRegistry $doctrine,
        PetitionRepository $petitionRepository,
        SignatureRepository $signatureRepository,
        YamlPetitionRepository $yamlPetitionRepository,
        ObfuscatedSignatureRepository $obfuscatedSignatureRepository,
        ConfirmationMessageFieldSetSelector $fieldsetSelector,
        LoggerInterface $logger,
        string $projectDir,
        array $metaConversionApiConfig
    ) {
        $this->doctrine = $doctrine;
        $this->petitionRepository = $petitionRepository;
        $this->signatureRepository = $signatureRepository;
        $this->yamlPetitionRepository = $yamlPetitionRepository;
        $this->obfuscatedSignatureRepository = $obfuscatedSignatureRepository;
        $this->fieldsetSelector = $fieldsetSelector;
        $this->projectDir = $projectDir;
        $this->logger = $logger;
        $this->metaConversionApiConfig = array_change_key_case($metaConversionApiConfig, CASE_LOWER);
    }

    /**
     * Creates a new signature for a given petition.
     * This is a JSON API and/or web/HTML endpoint.
     *
     * @Route(
     *     name="api_petition_sign",
     *     path="/api/petitions/{petitionId<[1-9]\d*>}/signatures",
     *     methods={"POST"}
     * )
     *
     * Disable RateLimit until it's made IPv6 compatible on local-filesystem storage
     * (https://github.com/jaytaph/RateLimitBundle/issues/115)
     * RateLimit(methods={"PUT", "POST"}, limit=35, period=3600)
     *
     * @param string $petitionId Petition identifier
     * @param LoggerInterface $logger
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function apiSignaturePost(string $petitionId, LoggerInterface $logger, SerializerInterface $serializer, ValidatorInterface $validator): Response
    {
        $em = $this->doctrine->getManager();
        $request = $this->getRequest();

        // get the Petition object
        $petition = $this->petitionRepository->find($petitionId);
        // By default, error redirection point to the referer
        $this->redirectError = $request->headers->get('referer');

        if (!$petition) {
            return $this->httpFail($request, ['This petition ID does not exist'], 404);
        }
        // deserialize
        $data = $serializer->deserialize($request->getContent(), Signature::class, $request->getContentType());

        // deserialize the parameters that are not in the App\Entiy\Signature object
        // (newsletter, recaptcha_token)
        $obj = $serializer->deserialize($request->getContent(), SignatureObject::class, $request->getContentType());

        // Store the page to redirect in case of error of a form POST payload.
        if ($obj) {
            $this->redirectError = filter_var($obj->getRedirectError(), FILTER_VALIDATE_URL)
                                 ?: filter_var($obj->getRedirect(), FILTER_VALIDATE_URL);
        }

        if ($obj->getMolassesPot()) { // A spammer/bot filled the hidden "molassepot" field and got stuck.
            return $this->httpFail($request, ['Request stuck (err 19190115)'], 400);
        }

        // clean email address
        $email = $data->getEmail();
        if ($email) {
            $email = self::cleanEmail($email);
            $data->setEmail($email);
        }

        // clean some properties
        $data->setPetition($petition);
        $data->setIpAddress($request->getClientIp());
        $data->setStatus('accepted');
        $data->setConfirmEmailStatus(Signature::EMAIL_TODO);
        $data->setNewsletterStatus($obj->getNewsletter() ? 'todo' : 'ignore');
        $data->setCountry(strtoupper(str_replace('_', '', $data->getCountry() ?: '')));

        $languages = substr(implode(',', array_map(function ($e) {
            return substr($e, 0, 8);
        }, $request->getLanguages())), 0, 32);
        $data->setLanguages($languages);

        $known_properties = $this->getClassPrivateProperties(Signature::class, SignatureObject::class);
        $extra = $this->getExtraParameters($request, array_flip($known_properties));
        if (isset($extra['privacy']) && !preg_match('/^([01]|on|off)$/', $extra['privacy'])) {
            $logger->warning(sprintf('[spam] bot detected from %s submitted %s', $request->getClientIp(), $request->getContent()));
            return $this->httpSuccess($request, $petition, $data, $obj);
        }

        if ($extra) {
            // don't clutter the DB with (huge) captcha tokens
            if (!empty($extra['recaptcha_token'])) {
                $extra['recaptcha_token'] = strlen($extra['recaptcha_token']) . 'b';
            }
            $data->setExtra($extra);
        }

        // validate
        $errors = $validator->validate($data);
        if (count($errors) > 0) {
            $errorList = array_map(function ($error) {
                return $error->getPropertyPath() . ': ' . $error->getMessage();
            }, $errors->getIterator()->getArrayCopy());
            $logger->warning(sprintf('signature validation error: error=%s, petition=%d, email=%s', join(', ', $errorList), $petition->getId(), $data->getEmail()));
            return $this->httpFail($request, $errorList, 400, $petition, $data);
        }

        // validate recpatcha
        if ($this->shouldValidateRecaptcha($petition)) {
            $recaptchaToken = $obj->getRecaptchaToken();
            if (empty($recaptchaToken)) {
                $logger->warning(sprintf('signature validation error: empty reCaptcha, petition=%d, email=%s', $petition->getId(), $data->getEmail()));
                return $this->httpFail($request, ['empty reCaptcha validation failed'], 400, $petition, $data);
            }
            if (!$this->validateReCaptcha($recaptchaToken, $request, $logger)) {
                $logger->warning(sprintf('signature validation error: invalid reCaptcha token length=%d petition=%d, email=%s', strlen($recaptchaToken), $petition->getId(), $data->getEmail()));
                return $this->httpFail($request, ['reCaptcha validation failed'], 400, $petition, $data);
            }
        }

        // lookup for existing signature
        $signature = $this->signatureRepository->findExisting($email, $petition);

        /**
         * No signature pre-existed, then persist it.
         */
        if (!$signature) {
            // Look for an existing but archived signature.
            // There, email addressses are normalized (lowercase, no "+" suffixes) and
            // @gmail.com must have been cleaned from their dots.
            $isArchivedSignature = $this->obfuscatedSignatureRepository->findByPetitionAndEmail(
                $petition,
                self::cleanEmail($email, true)
            );

            if ($isArchivedSignature) {
                // Just no-op. Continue outside the conditional straight to JSON output codepath.
                $logger->info(
                    'Found an archived signature matching petition={p}, email={email}',
                    ['p' => $petition->getSlug(), 'email' => $data->getEmail()]
                );
            } else {
                // persist the data
                $em->persist($data);
                $em->flush();
                $logger->info('Signature added id={id}, email={email}', ['id' => $data->getId(), 'email' => $data->getEmail()]);

                $petitionExtra = $this->yamlPetitionRepository->find($petitionId)->getExtra();
                $country = !empty($petitionExtra) ? $petitionExtra['country'] : null;

                BaseWebtrackCommand::track(
                    $this->metaConversionApiConfig,
                    $this->projectDir,
                    'webtrack-cygne',
                    $data->getId(),
                    $country,
                    $request->headers->get('User-Agent'),
                    $this->logger
                );
            }

            return $this->httpSuccess($request, $petition, $data, $obj);
        }


        /**
         * An already stored signature pre-existed.
         */

        /**
         * Case 1: That preexisting signature was revoked.
         * Make it accepted again (it's possible that some regretted to have revoked)
         *  also make sure we send the confirmation email again (only if revoked/bounced)
         */
        if (in_array($signature->getStatus(), ['revoked', 'bounced'])) {
            $signature->setStatus('accepted');
            $signature->setConfirmEmailStatus(Signature::EMAIL_TODO);
            $em->flush();
        }

        /**
         * Case 2: That preexisting signature matches to the freshly submitted one,
         * Then assume it comes from the same people.
         */
        if (self::signatureEquals($data, $signature)) {
            $sameIps = $data->getIpAddress() === $signature->getIpAddress();
            // return the existing signature instead of the (identical) new one
            $logger->info(
                'Found that a signature matching petition={p}, email={email} is roughly equal',
                ['p' => $petition->getSlug(), 'email' => $data->getEmail()]
            );

            $signature->setNewsletterStatus($data->getNewsletterStatus());
            // When refreshing an "old" signature (more than 3 days ?), update the data *but*
            // also trigger a new thank-you email.
            // Do it if the IP addresses don't match (as an early-alert in case of spoofing)
            if (!$sameIps || $signature->getCreatedAt() < new \DateTime(self::RESYNC_NEW_SUBMISSION_AFTER_PERIOD)) {
                $signature->setConfirmEmailStatus(Signature::EMAIL_TODO);
            }

            $data = $signature;
            /**
             * This field allows for construct like
             * `{{ signature.extra.many_attempts | default(0) }}` inside a Twig template
             * or
             * `s.getExtra("many_attempts") == 1` inside a petition's `email_template_paths`
             */
            $extra = $signature->getExtra();
            $extra['many_attempts'] = 1;

            $signature->setExtra($extra);
            // persist the data
            $em->persist($data);
            $em->flush();
            $logger->info('Signature updated id={id}, email={email}', ['id' => $data->getId(), 'email' => $data->getEmail()]);
        }

        /**
         * Case 3: That preexisting signature was significantly different from the
         * fresh one.
         * Then do not take the risk to update existing data from someone else who's
         * not legitimate (could be a fake intended to obtain a proof of signature existence)
         * and do nothing. Reply as if it was successfully inserted.
         */

        // In any case
        return $this->httpSuccess($request, $petition, $data, $obj);
    }

    /**
     * Returns a signature.
     * This is a JSON API endpoint.
     *
     * @Route(
     *     name="signature",
     *     path="/api/petitions/{petitionId<[1-9]\d*>}/signatures/{signatureId<[a-z0-9_@+.-]+>}",
     *     methods={"GET"}
     * )
     * @IsGranted("ROLE_CHECK", message="Access denied")
     *
     * @param string $petitionId Petition identifier
     * @param string $signatureId Signature identifier
     * @return JsonResponse
     */
    public function signature(string $petitionId, string $signatureId): JsonResponse
    {
        $request = $this->getRequest();
        $signature = null;

        // get the Petition object
        $petition = $this->petitionRepository->find($petitionId);
        if (!$petition) {
            return $this->httpFail($request, [ sprintf('Petition ID %d does not exist', (int)$petitionId) ], 404);
        }

        // get the Signature object by ID
        if (is_numeric($signatureId) && (int)$signatureId > 0) {
            $signature = $this->signatureRepository->findOneBy([
                'id' => (int)$signatureId,
                'petition' => $petition,
            ]);
            if (!$signature) {
                return $this->httpFail($request, [ sprintf('Signature ID %d does not exist for petition ID %d', (int)$signatureId, $petition->getId()) ], 404);
            }
        }
        // get the Signature object by email address
        else {
            $signature = $this->signatureRepository->findExisting(
                self::cleanEmail($signatureId),
                $petition
            );

            if (!$signature) {
                $isArchivedSignature = $this->obfuscatedSignatureRepository
                                            ->findByPetitionAndEmail(
                                                $petition,
                                                self::cleanEmail($signatureId, true)
                                            );

                if ($isArchivedSignature) {
                    return $this->httpFail($request, ['status' => 'exists', 'email' => $signatureId], 400);
                }

                return $this->httpFail($request, [ sprintf('Signature with email %s does not exist for petition ID %d', $signatureId, $petition->getId()) ], 404);
            }
        }

        return new JsonResponse([
            'status'        => $signature->getStatus(),
            'firstName'     => $signature->getFirstName(),
            'lastName'      => $signature->getLastName(),
            'email'         => $signature->getEmail(),
            'city'          => $signature->getCity(),
            'postalCode'    => $signature->getPostalCode(),
            'country'       => $signature->getCountry(),
            'newsletter'    => $signature->getNewsletterStatus(),
        ]);
    }

    /**
     * Revokes a signature.
     * This is a web/HTML endpoint.
     *
     * @Route(
     *     name="signature_revoke",
     *     path="/petitions/{petitionSlug}/revoke",
     * )
     *
     * @param string $petitionSlug Petition slug
     * @param Request $request
     * @return Response
     */
    public function revoke(string $petitionSlug, Request $request, Revokator $revokator, TranslatorInterface $translator): Response
    {
        $signature = null;
        $translator->setLocale($request->getPreferredLanguage($this->getParameter('supported_language')) ?: 'en_US');

        $petition = $this->petitionRepository->findOneBy(['slug' => $petitionSlug]);
        if (!$petition) {
            throw new NotFoundHttpException($translator->trans('Petition does not exist'));
        }

        // we want to instantly return a 404 if there's no email param
        if ($request->getMethod() === 'GET' && empty($request->query->get('email'))) {
            throw new NotFoundHttpException($translator->trans('Signature can not be found'));
        }

        // load form data from query params
        $defaultData = [
            'email' => $request->query->get('email'),
            'token' => $request->query->get('token'),
        ];

        // create form
        $form = $this->createFormBuilder($defaultData)
            ->add('email', Type\HiddenType::class, [
                'constraints' => [ new Constraints\NotBlank() ]
            ])
            ->add('token', Type\HiddenType::class, [
                'constraints' => [ new Constraints\NotBlank() ]
            ])
            ->add('send', Type\SubmitType::class, [
                'label' => $translator->trans('Discard my signature'),
                'attr' => [ 'class' => 'btn-danger' ],
            ])
            ->getForm();
        $form->handleRequest($request);

        // validate form
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            // we validate the revoke token
            // note: it's important that we check the signature before querying the database by email
            // as it's a performance improvement, and prevents timing attacks to guess
            // whether an email has signed a petition
            if ($data['token'] !== $revokator->getRevokeTokenSign($petition->getId(), $data['email'])) {
                $form->addError(new FormError($translator->trans('Invalid token')));
            } else {
                $signature = $this->signatureRepository->findExisting(
                    self::cleanEmail($data['email']),
                    $petition
                );

                if (!$signature) {
                    $isArchivedSignature = $this->obfuscatedSignatureRepository
                                                ->findByPetitionAndEmail(
                                                    $petition,
                                                    self::cleanEmail($data['email'], true)
                                                );
                    $form->addError(new FormError($translator->trans(
                        $isArchivedSignature
                        ? "Signature has already been handed over or archived. Can't revoke, please contact us."
                        : "Signature not found"
                    )));
                } else {
                    // mark signature as revoked
                    $signature->setStatus('revoked');
                    $signature->setRevokedAt(new \DateTime('now'));

                    // save to database
                    $em = $this->doctrine->getManager();
                    $em->flush();

                    return $this->redirectToRoute('signature_revoke_confirm', [
                        'petitionSlug' => $petition->getSlug()
                    ]);
                }
            }
        } else {
            if ($request->query->get('token') === $revokator->getRevokeTokenSign($petition->getId(), $request->query->get('email'))) {
                $signature = $this->signatureRepository->findExisting(
                    self::cleanEmail($request->query->get('email')),
                    $petition
                );
            }
        }

        $petition_page = null;
        if ($signature) {
            $fieldset = $this->fieldsetSelector->get($petition, $signature->getExtra()['variant'] ?? false);
            $petition_page = $fieldset->getFormPageUrl();
        }

        return $this->render('revoke.html.twig', array(
            'petition' => $petition,
            'signature' => $signature,
            'petition_page' => $petition_page,
            'form' => $form->createView(),
        ));
    }

    /**
     * Confirms the signature revocation.
     * This is a web/HTML endpoint.
     *
     * @Route(
     *     name="signature_revoke_confirm",
     *     path="/petitions/{petitionSlug}/revoke/confirm",
     * )
     *
     * @param string $petitionSlug
     * @return Response
     */
    public function revokeConfirm(string $petitionSlug, TranslatorInterface $translator, Request $request): Response
    {
        $petition = $this->petitionRepository->findOneBy(['slug' => $petitionSlug]);
        if (!$petition) {
            throw new NotFoundHttpException($translator->trans('Petition does not exist'));
        }

        $translator->setLocale($request->getPreferredLanguage($this->getParameter('supported_language')) ?: 'en_US');
        return $this->render('revoke-confirm.html.twig', array(
            'petition' => $petition,
        ));
    }

    //
    // Helpers
    //

    protected function getRequest(): Request
    {
        return $this->container->get('request_stack')->getCurrentRequest();
    }

    public static function cleanEmail(string $email, $clean_gmail_dot = false): string
    {
        $email = strtolower(trim($email));
        $m = [];
        if (preg_match('/([^\+\@]+)(?:\+[^@]+)?\@(.*)/', $email, $m)) {
            $email = sprintf('%s@%s', $m[1], $m[2]);
        }

        if ($clean_gmail_dot) {
            preg_match('/(.*)@(.*)/', $email, $m);
            if ($m[2] == 'gmail.com') {
                $email = str_replace('.', '', $m[1]) . '@gmail.com';
            }
        }

        return $email;
    }

    /**
     * Returns query-string keys which must be ignored when processing extra parameters
     * because they already got considered by these classes deserializers.
     *
     * @return array classes' private properties
     */
    protected function getClassPrivateProperties(...$existing_classes): array
    {
        $keys = array_merge(...array_map(function ($e) {
            $reflection = new \ReflectionClass($e);
            $vars = $reflection->getProperties(\ReflectionProperty::IS_PRIVATE);
            return array_map(function ($f) {
                return $f->name;
            }, $vars);
        }, $existing_classes));
        return array_filter($keys, function ($e) {
            return !in_array($e, ['extra'], true);
        });
    }

    /**
     * Collects unknown POST/JSON parameters.
     * @return Array of parameters
     */
    protected function getExtraParameters(Request $request, $known_properties): array
    {
        $params = ($request->getContentType() === 'json')
                ? @json_decode($request->getContent(), true)
                : $request->request->all();
        // filter
        $params = array_diff_key($params, $known_properties);
        // limit size
        while (strlen(serialize($params)) > self::EXTRA_PAYLOAD_LIMIT) {
            array_pop($params);
        }
        return $params;
    }

    /**
     * Compare the properties of two signatures.
     */
    protected static function signatureEquals(Signature $s1, Signature $s2, array $ignore = self::SIGNATURE_COMPARISON_OMITTED_FIELDS): bool
    {
        $defaultContext = [AbstractNormalizer::IGNORED_ATTRIBUTES => $ignore];
        $serializer = new Serializer([new DateTimeNormalizer(),
                                      new ObjectNormalizer(null, null, null, null, null, null, $defaultContext)]);
        $diff = self::array_diff_assoc_recursive(
            $serializer->normalize($s1, null, ['skip_null_values' => true]),
            $serializer->normalize($s2, null, ['skip_null_values' => true])
        );
        return count($diff) === 0;
    }

    protected static function array_diff_assoc_recursive($array1, $array2)
    {
        $difference = [];
        foreach ($array1 as $key => $value) {
            if (is_array($value)) {
                if (!isset($array2[$key]) || !is_array($array2[$key])) {
                    $difference[$key] = $value;
                } else {
                    $new_diff = self::array_diff_assoc_recursive($value, $array2[$key]);
                    if (!empty($new_diff)) {
                        $difference[$key] = $new_diff;
                    }
                }
            } else {
                if (!array_key_exists($key, $array2) || $array2[$key] !== $value) {
                    $difference[$key] = $value;
                }
            }
        }
        return $difference;
    }

    protected function httpFail(Request $request, array $errors, int $code, Petition $petition = null, $data = null)
    {
        if (
            !in_array("application/json", $request->getAcceptableContentTypes())
            && $request->getContentType() === 'form'
        ) {
            if (!$this->redirectError && $petition) {
                $fieldset = $this->fieldsetSelector->get($petition, $data->getExtra()['variant'] ?? false);
                $this->redirectError = $fieldset->getFormPageUrl();
            }
            if (!$this->redirectError) {
                return $this->redirectToRoute('index');
            }

            $return = str_replace(['%errors%', '%success%'], [urlencode(implode(',', $errors)), '0'], $this->redirectError);
            return $this->redirect($return);
        }

        return $this->json(['status' => 'failed', 'errors' => $errors], $code);
    }


    protected function httpSuccess(Request $request, Petition $petition, $data, $obj)
    {
        /*
          if Content-Type is "form" and Accept is not JSON, redirect to either the requested
          success URL or the "share site URL"
          The case is rare enough to allow ourselves to load the fieldset selector (and petitions.yaml file)
          when this happens
        */
        if (
            !in_array("application/json", $request->getAcceptableContentTypes())
            && $request->getContentType() === 'form'
        ) {
            $fieldset = $this->fieldsetSelector->get($petition, $data->getExtra()['variant'] ?? false);
            $petition_page = $fieldset->getFormPageUrl();

            $return = filter_var($obj->getRedirectSuccess(), FILTER_VALIDATE_URL)
                    ?: filter_var($obj->getRedirect(), FILTER_VALIDATE_URL)
                    ?: $petition_page;
            return $this->redirect(str_replace('%success%', '1', $return));
        }

        return new JsonResponse([
            'status'        => $data->getStatus(),
            'firstName'     => $data->getFirstName(),
            'lastName'      => $data->getLastName(),
            'email'         => $data->getEmail(),
            'city'          => $data->getCity(),
            'postalCode'    => $data->getPostalCode(),
            'country'       => $data->getCountry(),
            'newsletter'    => $obj->getNewsletter(),
        ], 201);
    }

    public function shouldValidateRecaptcha($petition)
    {
        return (
            $this->getParameter('recaptcha_enabled') || in_array('recaptcha', $petition->getRequirements())
        ); // && !$this->getRequest()->headers->get('x-no-captcha'); // For testing purpose
    }

    //
    // Verify reCaptcha
    //
    private function validateReCaptcha(string $token, Request $request, LoggerInterface $logger): bool
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // body
        $body = [
            "response"  => $token,
            "secret"    => $this->getParameter('recaptcha_secret'),
            "remoteip"  => $request->getClientIp(),
        ];
        $body = http_build_query($body);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $body);

        // send
        $resp = curl_exec($ch);
        curl_close($ch);

        if (!$resp) {
            $logger->warning('reCaptcha: Request to recaptcha service failed');
            return false;
        }
        $d = json_decode($resp, true);

        if (!$d || $d['success'] !== true) {
            $errors = $d['error-codes'];
            $logger->warning('reCaptcha: validation failed: ' . join(', ', $errors));
            return false;
        }

        return true;
    }
}
