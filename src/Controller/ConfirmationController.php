<?php

namespace App\Controller;

use App\Entity\Petition;
use App\Entity\Signature;
use App\Exception\StatusMismatchException;
use App\Service\ConfirmationMessageBuilder;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\Transport\TransportInterface;
use Symfony\Component\Mailer\Exception\HttpTransportException;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Error\LoaderError;

class ConfirmationController extends AbstractController
{
    private $em;
    private $confirmation_message_builder;
    private $mailer;
    private $logger;
    private $doctrine;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(
        EntityManagerInterface $em,
        ConfirmationMessageBuilder $confirmation_message_builder,
        ManagerRegistry $doctrine,
        TransportInterface $mailer,
        LoggerInterface $logger
    ) {
        $this->em  = $em;
        $this->doctrine = $doctrine;
        $this->mailer = $mailer;
        $this->confirmation_message_builder = $confirmation_message_builder;
        $this->logger = $logger;
    }

    public function getConfirmationEmailInformation(Signature $signature, string &$template)
    {
        // Lets exception flow one level up.
        $template = $this->confirmation_message_builder->getTemplatePath($signature);
        $html = $this->confirmation_message_builder->getHtml($signature, $template);
        if (!$html) {
            return false; // No email must be sent
        }

        return $this->confirmation_message_builder->getEmailMessage($signature, $html);
    }

    /**
     * Sends all pending email confirmation signatures (confirmEmailStatus = `todo`).
     */
    public function processMultiple($signatures): array
    {
        $statuses = [];
        foreach ($signatures as $signature) {
            try {
                $result = $this->processOne($signature->getId());
                $statuses[$result] = 1 + ($statuses[$result] ?? 0);
            } catch (StatusMismatchException $e) {
                // ignore exception
            } catch (\Exception $e) {
                $this->logger->warning(
                    sprintf(
                        '[confirmation-email] Exception raised during loop, ID=%s: %s',
                        $signature->getId(),
                        $e->getMessage()
                    )
                );
                $statuses['exception'] = 1 + ($statuses['exception'] ?? 0);
                break; // Stop processing as soon as an exception arises.
            }
        }

        return $statuses;
    }

    /**
     * Sends email confirmation signature for a given signature ID.
     * The strategy is to run everything inside a SQL transaction, with
     * a write (exclusive) lock.
     *
     * @param int $signatureId A signature id
     */
    private function processOne(int $signatureId)
    {
        $em = $this->doctrine->getManager();
        $logger = $this->logger;
        return $em->transactional(function($em) use ($signatureId, $logger) {
            // get Signature entity in the transaction (SELECT ... FOR UPDATE)
            $signature = $em->find('App\Entity\Signature', $signatureId, \Doctrine\DBAL\LockMode::PESSIMISTIC_WRITE);

            // ensure compatible status (only "todo" is acceptable)
            if ($signature->getConfirmEmailStatus() !== Signature::EMAIL_TODO) {
                // raising an exception will rollback the transaction
                throw new StatusMismatchException('Status mismatch');
            }

            try {
                // get email's HTML body
                $template = $this->confirmation_message_builder->getTemplatePath($signature);
                if ($template === false) {
                    $signature->setConfirmEmailStatus(Signature::EMAIL_IGNORE);
                    $signature->setConfirmEmailId(null);
                    $signature->setConfirmEmailError('no template');
                    return 'done';
                }

                $html = $this->confirmation_message_builder->getHtml($signature, $template);
                if ($html === false) {
                    $signature->setConfirmEmailStatus(Signature::EMAIL_IGNORE);
                    $signature->setConfirmEmailId(null);
                    $signature->setConfirmEmailError('no markup');
                    return 'done';
                }
                // send email
                $message = $this->confirmation_message_builder->getEmailMessage($signature, $html);
                $sentMessage = $this->mailer->send($message);
            } catch (LoaderError $e) {
                $signature->setConfirmEmailStatus(Signature::EMAIL_ERROR);
                $signature->setConfirmEmailId(null);
                $signature->setConfirmEmailError('template-exception:' . $e->getMessage());
                return 'template-exception';
            } catch (HttpTransportException | \Exception $e) {
                // exception was raised, log it and save `error` status
                $logger->warning(
                    sprintf(
                        '[confirmation-email] Exception raised, ID=%s: %s',
                        $signature->getId(),
                        $e->getMessage()
                    )
                );

                $signature->setConfirmEmailStatus(Signature::EMAIL_ERROR);
                $signature->setConfirmEmailId(null);
                $signature->setConfirmEmailError('exception:' . $e->getMessage());
                return 'exception';
            }

            /**
             * Using symfony/mailer, a message is assumed sent *unless* an exception was returned
             * As such there is no AWS-specific "rejectReason".
             */
            $messageId = $sentMessage->getMessageId();
            $logger->debug(
                '[confirmation-email] Sent to {email}, message-id={id} success',
                ['id' => $messageId, 'email' => $signature->getEmail()]
            );

            // change the status
            $signature->setConfirmEmailStatus(Signature::EMAIL_SENT);
            $signature->setConfirmEmailId($messageId);
            $signature->setConfirmEmailError(null);
            return 'done';
        });
    }
}
