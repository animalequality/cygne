<?php

namespace App\Controller;

use App\Controller\SnsBaseController;
use App\Entity\Signature;
use Aws\Sns\Message;
use Aws\Sns\MessageValidator;
use Aws\Sns\SnsClient;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class SnsController extends SnsBaseController
{

    private $doctrine = null;

    public function __construct(
        ManagerRegistry $doctrine,
        SnsClient $snsClient,
        MessageValidator $messageValidator,
        LoggerInterface $logger
    ) {
        $this->doctrine = $doctrine;
        parent::__construct($snsClient, $messageValidator, $logger);
    }

    /**
     * Complements to constructor by adding ability to set topics from environement.
     *
     * @param string $allowedTopicsArn Comma-separated list of topic to listen for.
     * @required
     */
    public function setTopics(string $allowedTopicsArn)
    {
        $this->allowedTopicsArn = array_filter(explode(',', $allowedTopicsArn));
    }

    /**
     * Receives a webhook notification event from Amazon SNS.
     * This is a webhook endpoint.
     *
     * @Route(name="webhook_sns", path="/_webhooks/amazon-sns", methods={"POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function processRequest(Request $request): Response
    {
        // only for annotation override
        return parent::processRequest($request);
    }

    // We override processAny to store $signature property, later used by bounce/complaint handlers
    public function processAny(Message $message, array $data, string $message_id)
    {
        $type = $data['notificationType'] ?? $data['eventType'] ?? null;
        $this->signature = $this->doctrine->getRepository(Signature::class)->findOneBy(['confirmEmailId' => $message_id]);
        $recipients = $data['complaint']['complainedRecipients'] ?? $data['bounce']['bouncedRecipients'] ?? $data['delivery']['recipients'] ?? [];
        $recipients = implode(
            ', ',
            array_map(function ($email) {
                return is_string($email) ? $email : ($email['emailAddress'] ?? '');
            }, $recipients)
        );
        $logs = ['type' => $type, 'id' => $message_id, 'recipient' => $recipients];

        if (! $this->signature) {
            $this->logger->warning('unmatched SNS {type} for Message-ID {id} [{recipient}]', $logs);
            if (! $recipients) {
                $this->logger->warning(json_encode($data));
            }
            return new Response('Message-ID not bound to a signature.', 403);
        }

        $this->logger->info('SNS {type} for Message-ID {id} [{recipient}]', $logs);
        if (! $recipients) {
            $this->logger->warning(json_encode($data));
        }
        return parent::processAny($message, $data, $message_id);
    }

    public function processBounce(Message $message, array $data): Response
    {
        $bounce = $data['bounce'];
        // https://docs.aws.amazon.com/ses/latest/DeveloperGuide/notification-contents.html#bounce-types
        $this->markBounced('bounced:' . $bounce['bounceType'] . ':' . $bounce['bounceSubType']);
        return new Response('', 200);
    }

    public function processComplaint(Message $message, array $data): Response
    {
        $complaint = $data['complaint'];
        // https://docs.aws.amazon.com/ses/latest/DeveloperGuide/notification-contents.html#complaint-object
        $this->markBounced('complaint:' . ($complaint['complaintFeedbackType'] ?? json_encode($complaint)));
        return new Response('', 200);
    }

    public function processDelivery(Message $message, array $data): Response
    {
        $this->signature->setConfirmEmailStatus(Signature::EMAIL_DELIVERED);
        $this->doctrine->getManager()->flush();
        return new Response('', 200);
    }

    private function markBounced(string $msg)
    {
        $this->signature->setStatus('bounced');
        $this->signature->setRevokedAt(new \DateTime('now'));
        $this->signature->setConfirmEmailError($msg);
        $this->signature->setConfirmEmailStatus(Signature::EMAIL_ERROR);
        $this->doctrine->getManager()->flush();
    }
}
