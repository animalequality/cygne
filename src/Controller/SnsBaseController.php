<?php

namespace App\Controller;

use Aws\Sns\Message;
use Aws\Sns\MessageValidator;
use Aws\Sns\SnsClient;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;

abstract class SnsBaseController extends AbstractController
{

    const HEADER_TYPE_NOTIFICATION          = 'Notification';
    const HEADER_TYPE_CONFIRM_SUBSCRIPTION  = 'SubscriptionConfirmation';
    const MESSAGE_TYPE_SUBSCRIPTION_SUCCESS = 'AmazonSnsSubscriptionSucceeded';
    const MESSAGE_TYPE_BOUNCE               = 'Bounce';
    const MESSAGE_TYPE_COMPLAINT            = 'Complaint';
    const MESSAGE_TYPE_DELIVERY             = 'Delivery';

    public $allowedTopicsArn = [];

    private $snsClient;
    private $messageValidator;
    protected $logger;

    /**
     * @param SnsClient $snsClient
     * @param MessageValidator $messageValidator
     * @param LoggerInterface $logger
     */
    public function __construct(SnsClient $snsClient, MessageValidator $messageValidator, LoggerInterface $logger)
    {
        $this->snsClient = $snsClient;
        $this->messageValidator = $messageValidator;
        $this->logger = $logger;
    }

    /**
     * Receives a webhook notification event from Amazon SNS.
     * This is a webhook endpoint.
     *
     * @Route(name="basesns_endpoint", path="/_default/sns/endpoint", methods={"POST"})
     *
     * @param Request $request
     * @return Response
     */
    public function processRequest(Request $request): Response
    {
        $messageTypeHeader = $request->headers->get('x-amz-sns-message-type');
        $content = $request->getContent();
        if (null === $messageTypeHeader || !$content) {
            throw new BadRequestHttpException('Invalid SNS request');
        }

        $message = new Message(json_decode($content, true));

        if (! in_array($messageTypeHeader, [self::HEADER_TYPE_NOTIFICATION, self::HEADER_TYPE_CONFIRM_SUBSCRIPTION], TRUE)) {
            throw new \RuntimeException(sprintf('Invalid SNS header: "%s"', $messageTypeHeader));
        }

        if (false === $this->messageValidator->isValid($message)) {
            return new Response('Invalid SNS message.', 403);
        }

        if ($messageTypeHeader === self::HEADER_TYPE_NOTIFICATION) {
            return $this->processNotification($message);
        }
        if ($messageTypeHeader === self::HEADER_TYPE_CONFIRM_SUBSCRIPTION) {
            return $this->processSubscription($message);
        }
    }

    public function processSubscription(Message $message): Response
    {
        $arn = $message->offsetGet('TopicArn');
        if (! in_array($arn, $this->allowedTopicsArn)) {
            return new Response('Topic not found', 404);
        }

        $this->snsClient->confirmSubscription([
            'TopicArn' => $arn,
            'Token'    => $message->offsetGet('Token'),
        ]);

        return new Response('OK', 200);
    }

    public function processNotification(Message $message): Response
    {
        if (! $message->offsetGet('MessageId')) {
            return new Response('SNS does not contain top-level MessageID', 403);
        }

        $notificationData = json_decode($message->offsetGet('Message'), true);
        $type = $notificationData['notificationType'] ?? $notificationData['eventType'] ?? false;
        if (! $type) {
            return new Response('SNS payload missed notificationType or eventType.', 403);
        }

        if (self::MESSAGE_TYPE_SUBSCRIPTION_SUCCESS === $type) {
            $confirmation_message = $notificationData['message'];
            $this->logger->debug('SNS {type} : {message}', ['type' => $type, 'message' => $confirmation_message]);
            return new Response('OK', 200);
        }

        if (! isset($notificationData['mail']['messageId'])) {
            return new Response('SNS payload missed source messageId.', 403);
        }

        // this is the original, Amazon SES attributed, Message-Id
        $message_id = $notificationData['mail']['messageId'];

        $r = $this->processAny($message, $notificationData, $message_id);
        if ($r instanceof Response) {
            return $r;
        }

        switch ($type) {
            case self::MESSAGE_TYPE_BOUNCE:
                return $this->processBounce($message, $notificationData);
            case self::MESSAGE_TYPE_COMPLAINT:
                return $this->processComplaint($message, $notificationData);
            case self::MESSAGE_TYPE_DELIVERY:
                return $this->processDelivery($message, $notificationData);
            default:
                return new Response('Notification type not understood', 403);
        }
    }

    public function processAny(Message $message, array $data, string $message_id)
    {
        return true; // no-op
    }

    public function processBounce(Message $message, array $data): Response
    {
        return new Response('', 200);
    }

    public function processComplaint(Message $message, array $data): Response
    {
        return new Response('', 200);
    }

    public function processDelivery(Message $message, array $data): Response
    {
        return new Response('', 200);
    }
}
