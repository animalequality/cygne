<?php

namespace App\Controller;

use App\Entity\Petition;
use App\Entity\Signature;
use App\Repository\SignatureRepository;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\YamlPetitionRepository;

class StatisticsController extends AbstractController
{

    // private const ARCHIVE_TIME = "2020-07-31 15:02:00"; // first round of archival
    private const ARCHIVE_TIME = "2024-11-01 00:00:01";    // second round of archival

    private const GROUPING_FIELD = 'country';

    /**
     * array $petitionGroups Petition classification maps (for grouped output).
     * This attribute is built from petitions.yaml and ends up like:
     * ['group1' => [1,3,6], 'group2' => [2,4,5]]
     */
    private $petitionGroups = null;

    /**
     * int $startDate Timestamp of the start date
     */
    private $startDate = false;

    /**
     * int $endDate Timestamp of the start date
     */
    private $endDate = false;

    /**
     * array<int> $petitionIds The list of petition IDs
     */
    private $petitionIds = false;

    /**
     * int $hasCountry Number of the petition ID which are actually country-codes
     */
    private $hasCountry = 0;

    /**
     * YamlPetitionRepository
     */
    private $yamlRepository = null;

    public function __construct(EntityManagerInterface $em, YamlPetitionRepository $yamlRepository, LoggerInterface $logger)
    {
        $this->em  = $em;
        $this->logger = $logger;
        $this->yamlRepository = $yamlRepository;
    }

    /**
     * Returns statistics by month
     * This is a JSON API endpoint.
     *
     * @Route(
     *     name="stats_by_month",
     *     path="/api/stats/by-month/{month<20[12]\d-[01]\d>}",
     *     methods={"GET"},
     * )
     * @IsGranted("ROLE_CHECK", message="Access denied")
     *
     * @param string $month Month in format YYYY-MM
     * @return Response
     */
    public function statsByMonth(Request $request, SignatureRepository $repo, string $month): Response
    {
        $this->startDate = strtotime($month);
        $this->endDate = strtotime($month . ' + 1 month');
        $nlStatus = $request->query->get('nl_status');
        $wantsArchive = null;
        if ($request->query->has('with_archive')) {
            $wantsArchive = intval($request->query->get('with_archive'));
        }
        $useArchive = strtotime(self::ARCHIVE_TIME) - $this->startDate > 1 && $wantsArchive !== 0;

        if ($useArchive && $nlStatus) {
            return $this->json(['status' => 'failed', 'errors' => ['Can\'t request subscribers status prior to ' . self::ARCHIVE_TIME]], 400);
        }

        $this->getPetitionIds($request);
        $stats = $this->getStatsByPetitionIds($repo, null, $nlStatus, $useArchive);

        return $this->format($request, $stats);
    }

    /**
     * Returns statistics for a range between two timestamps
     * This is a JSON API endpoint.
     *
     * @Route(
     *     name="stats_by_range",
     *     path="/api/stats/by-range/{from<\d{10}>}/{to<\d{10}>}",
     *     methods={"GET"},
     * )
     * @IsGranted("ROLE_CHECK", message="Access denied")
     *
     * @return Response
     */
    public function statsByRange(Request $request, SignatureRepository $repo, string $from, string $to): Response
    {
        $this->startDate = (int)$from;
        $this->endDate = (int)$to;
        $nlStatus = $request->query->get('nl_status');
        $wantsArchive = null;
        if ($request->query->has('with_archive')) {
            $wantsArchive = intval($request->query->get('with_archive'));
        }
        $useArchive = strtotime(self::ARCHIVE_TIME) - $this->startDate > 1 && $wantsArchive !== 0;

        if ($this->endDate >= time()) {
            if (($this->endDate - time()) / 3600 > 72) {
                return $this->json(['status' => 'failed', 'errors' => ['End time is in the future']], 400);
            }
            // Assume end-time is `now`
            $this->endDate = time();
        }
        if ($this->endDate <= $this->startDate) {
            return $this->json(['status' => 'failed', 'errors' => ['Range beginning after range end or time is in the future']], 400);
        }

        if ($useArchive && $nlStatus) {
            return $this->json(['status' => 'failed', 'errors' => ['Can\'t request subscribers status prior to ' . self::ARCHIVE_TIME]], 400);
        }

        $this->getPetitionIds($request);
        $stats = $this->getStatsByPetitionIds($repo, null, $nlStatus, $useArchive);
        return $this->format($request, $stats);
    }

    /**
     * Returns statistics for a range between two date
     * This is a JSON API endpoint.
     *
     * @Route(
     *     name="stats_by_timestamp",
     *     path="/api/stats/by-range/{from<20[12]\d-[01]\d-[0-3]\d>}/{to<20[12]\d-[01]\d-[0-3]\d>}",
     *     methods={"GET"},
     * )
     * @IsGranted("ROLE_CHECK", message="Access denied")
     *
     * @return Response
     */
    public function statsByDateRange(Request $request, SignatureRepository $repo, string $from, string $to): Response
    {
        return $this->statsByRange($request, $repo, strtotime($from), strtotime($to));
    }

    public function getPetitionIds(Request $request): array
    {
        $ids = $request->query->get('id');
        $ids = array_filter(array_unique(explode(',', $ids)));
        rsort($ids);
        foreach ($ids as $id) {
            if (isset($this->getPetitionGroups()[$id])) {
                $this->hasCountry++;
                $ids = array_merge($ids, $this->getPetitionGroups()[$id]);
            }
        }
        $ids = array_unique(array_filter(array_map('intval', $ids)));
        sort($ids);

        if (empty($ids)) {
            $ids = array_map(
                function ($e) {
                    return $e->getId();
                },
                $this->em->getRepository(Petition::class)->findAll()
            );
            $this->hasCountry = 2;
        }

        $this->petitionIds = $ids;

        return $ids;
    }

    private function getPetitionGroups(): array
    {
        if ($this->petitionGroups !== null) {
            return $this->petitionGroups;
        }

        $configFile = sprintf('%s//config/petitions.yaml', $this->getParameter('kernel.project_dir'));
        $this->yamlRepository->setConfig([$configFile]);
        $yamlPetitions = $this->yamlRepository->findAll();
        if (!$yamlPetitions) {
            return [];
        }

        foreach ($yamlPetitions as $y) {
            if (is_string($y->getExtra()[self::GROUPING_FIELD] ?? false)) {
                $this->petitionGroups[$y->getExtra()[self::GROUPING_FIELD]][] = $y->getId();
            }
        }

        return $this->petitionGroups;
    }

    private function getStatsByPetitionIdsRequest(SignatureRepository $repo)
    {
        return $repo
            ->createQueryBuilder('s')
            ->select('IDENTITY(s.petition) as id', 'count(1) as c', 'p.slug')
            ->andWhere('s.petition IN (:petition_id)')
            ->andWhere('s.status = :status')
            ->andWhere('s.createdAt BETWEEN :startDate AND :endDate')
            ->leftJoin('s.petition', 'p')
            ->groupBy('s.petition')
            ->setParameter('petition_id', $this->petitionIds, Connection::PARAM_STR_ARRAY)
            ->setParameter('status', 'accepted')
            ->setParameter('startDate', date('Y-m-d', $this->startDate))
            ->setParameter('endDate', date('Y-m-d H:i:s', $this->endDate));
    }

    public function getStatsByPetitionIds(SignatureRepository $repo, $_req = null, $nlStatus = false, $useArchive = false): array
    {
        $_req = $_req ?: $this->getStatsByPetitionIdsRequest($repo);
        if ($nlStatus) {
            if (! in_array($nlStatus, ['done', 'ignore', 'todo', 'error'], true)) {
                $nlStatus = 'todo';
            }
            $_req->andWhere('s.newsletterStatus = :nlStatus')
                 ->setParameter('nlStatus', $nlStatus);
        }
        $req = $_req->getQuery();
        $results = [];
        foreach ($req->getArrayResult() as $k) {
            $results[$k['id']] = ['id' => intval($k['id']), 'count' => intval($k['c']), 'slug' => $k['slug']];
        }

        // with archive
        if ($useArchive) {
            $res = $repo->getArchivedSignaturesCountMulti($this->petitionIds);
            foreach ($res as $k) {
                $id = intval($k['id']);
                $results[$id] = ($results[$id] ?? []) + ['id' => $id, 'slug' => $k['slug'], 'count' => 0];
                $results[$id]['count'] = $results[$id]['count'] + intval($k['c']);
            }
        }

        return $results;
    }

    public function groupByCountry(array $json): array
    {
        $result = [];
        foreach ($json as $id => $value) {
            $found = false;
            foreach ($this->getPetitionGroups() as $cc => $ids) {
                if (in_array($id, $ids, true)) {
                    $result[$cc][$id] = $value;
                    $found = true;
                    break;
                }
            }
            if (! $found) {
                $result['NC'][$id] = $value;
            }
        }
        return $result;
    }

    public function format(Request $request, array $json): Response
    {
        $requestText = $request->query->get('text');
        $with_zero = $request->query->get('with_empty') != null;
        $requestGrouping = $request->query->get('group');
        $shouldGroup = in_array($requestGrouping, ['0', 'off', 'false'], true) ? false : ($requestGrouping ? true : $this->hasCountry > 1);
        $shouldText = in_array($requestText, ['0', 'off', 'false'], true) ? false : ($requestText ? true : $shouldGroup);

        if ($with_zero) {
            foreach ($this->petitionIds as $id) {
                $json[$id] = ($json[$id] ?? []) + ['id' => $id, 'count' => 0];
            }
        }

        $this->logger->info(
            'Request statistics with: text={rtext}, finally-text={shouldText}, zero={zero}, group={rgroup}, finally-group={shouldGroup}, ids={ids}',
            ['rtext' => (int)$requestText, 'shouldText' => (string)$shouldText, 'zero' => (int)$with_zero, 'rgroup' => (string)$requestGrouping,
             'shouldGroup' => (string)$shouldGroup, 'ids' => implode(',', $this->petitionIds)]
        );

        if (!$shouldGroup) {
            if (!$shouldText) {
                return $this->json($json);
            }

            // Ungroup text format
            uasort(
                $json,
                function ($a, $b) {
                    return $a['id'] > $b['id'];
                }
            );

            $output = array_map(
                function ($item) {
                    return sprintf("% 2d\t% 5d\t%s", $item['id'], $item['count'], $item['slug'] ?? '');
                },
                $json
            );
            $response = new Response(implode("\n", $output) . "\n");
            $response->headers->set('Content-Type', 'text/plain');
            return $response;
        }

        // Group by country
        $result = $this->groupByCountry($json);
        if (!$shouldText) {
            return $this->json($result);
        }

        // Grouped text-format
        $output = '';
        foreach ($result as $cc => $items) {
            $output .= sprintf(
                "## Results for %s between %s and %s\n",
                $cc,
                date('Y-m-d H:i:s', $this->startDate),
                date('Y-m-d H:i:s', $this->endDate)
            );
            uasort(
                $items,
                function ($a, $b) {
                    return $a['count'] < $b['count'];
                }
            );
            foreach ($items as $item) {
                $output .= sprintf("% 2d\t% 5d\t%s\n", $item['id'], $item['count'], $item['slug'] ?? '');
            }
        }

        $response = new Response($output);
        $response->headers->set('Content-Type', 'text/plain');
        return $response;
    }
}
