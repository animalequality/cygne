<?php

namespace App\Controller;

use App\Entity\Signature;
use Doctrine\Persistence\ManagerRegistry;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class MandrillWebhookController extends AbstractController
{

    private $doctrine = null;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    /**
     * Receives a webhook notification event from Mandrill.
     * This is a webhook endpoint (returns an empty response).
     *
     * @Route(name="webhook_mandrill", path="/_webhooks/mandrill", methods={"GET", "HEAD", "POST"})
     *
     * @param Request $request
     * @param LoggerInterface $logger)
     * @return Response
     */
    public function webhookMandrill(Request $request, LoggerInterface $logger): Response
    {
        if ($request->getMethod() !== 'POST') {
            return new Response('', 200, [
                'x-mandrill-webhook' => 'get-method-debug',
            ]);
        }

        // check signature
        if (!$this->checkMandrillWebhookSignature($request->headers->get('X-Mandrill-Signature'), $request->request->all())) {
            return new Response('Invalid Signature', Response::HTTP_UNAUTHORIZED);
        }

        // process events
        $str = $request->request->get('mandrill_events');
        $events = json_decode($str, true);
        foreach ($events as $event) {
            $this->processMandrillEvent($event, $logger);
        }

        // return HTTP 200
        return new Response('', 200, [
            'x-mandrill-webhook' => 'processed',
        ]);
    }

    private function processMandrillEvent(array $event, LoggerInterface $logger): bool
    {
        // make sure it's one of the rejected events
        $mandrillEventType = $event['event'];
        if (!in_array($mandrillEventType, ['hard_bounce', 'soft_bounce', 'reject'])) {
            $logger->warning('[Mandrill Webhook] Unacceptable Mandrill event type: ' . $mandrillEventType);
            return false;
        }

        // extract infos
        $mandrillMessageId = $event['msg']['_id'];
        $signatureId = $event['msg']['metadata']['signature_id'] ?? false;
        if (!$signatureId) {
            $logger->warning('[Mandrill Webhook] Missing signature_id Mandrill Message ID: ' . $mandrillMessageId);
            return false;
        }

        // query signature by ID
        $signature = $this->doctrine->getRepository(Signature::class)->find($signatureId);
        if (!$signature) {
            $logger->warning('[Mandrill Webhook] Cannot find signature ID: ' . $signatureId);
            return false;
        }

        // verify the Mandrill message ID (for safety)
        if ($signature->getConfirmEmailId() !== $mandrillMessageId) {
            $logger->warning('[Mandrill Webhook] Mandrill event ID mismatch: ' . $mandrillMessageId);
            return false;
        }

        // make as bounced
        $signature->setStatus('bounced');
        $signature->setRevokedAt(new \DateTime('now'));
        $signature->setConfirmEmailError($mandrillEventType);
        $this->doctrine->getManager()->flush();

        $logger->info(sprintf('[Mandrill Webhook][Mandrill: %s] Maked signature ID %s as bounced', $mandrillEventType, $signature->getId()));

        return true;
    }

    private function checkMandrillWebhookSignature($signature, $params)
    {
        $webhookUrl = $this->generateUrl(
            'webhook_mandrill',
            [],
            UrlGeneratorInterface::ABSOLUTE_URL
        );
        $webhookKey = $this->getParameter('mandrill_webhook_key');
        $expectedSignature = $this->generateMandrillWebhookSignature($webhookKey, $webhookUrl, $params);
        return trim($expectedSignature) === trim($signature);
    }

    /**
     * Generates a base64-encoded signature for a Mandrill webhook request.
     * Implementation from:
     * https://mandrill.zendesk.com/hc/en-us/articles/205583257-How-to-Authenticate-Webhook-Requests
     *
     * @param string $webhookKey the webhook's authentication key
     * @param string $webhookUrl the webhook url
     * @param array $params the request's POST parameters
     */
    private function generateMandrillWebhookSignature($webhookKey, $webhookUrl, $params)
    {
        $signedData = $webhookUrl;
        ksort($params);
        foreach ($params as $key => $value) {
            $signedData .= $key;
            $signedData .= $value;
        }
        return base64_encode(hash_hmac('sha1', $signedData, $webhookKey, true));
    }
}
