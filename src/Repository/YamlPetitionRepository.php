<?php

namespace App\Repository;

use Symfony\Component\Yaml\Yaml;

use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Normalizer\ArrayDenormalizer;
use Symfony\Component\Serializer\Normalizer\GetSetMethodNormalizer;
use Symfony\Component\Serializer\Normalizer\DateTimeNormalizer;
use Symfony\Component\Serializer\Encoder\YamlEncoder;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Psr\Cache\CacheItemPoolInterface;
use Psr\Log\LoggerInterface;
use App\Entity\Petition;

class YamlPetitionRepository extends ServiceEntityRepository
{

    /**
     * @var CacheItemPoolInterface
     */
    private $cache;
    /**
     * @var LoggerInterface
     */
    private $logger;

    public const STORAGE_KEY = 'petitions-yaml-cached';

    private $initialized = false;
    private $petitions = [];
    private $petitionsBySlug = [];

    protected $defaultFiles = [];

    public function __construct(
        ManagerRegistry $registry,
        LoggerInterface $logger,
        CacheItemPoolInterface $cache,
        array $defaultFiles)
    {
        $this->cache = $cache;
        $this->logger = $logger;
        $this->defaultFiles = $defaultFiles;
        parent::__construct($registry, Petition::class);
    }

    public function setConfig(array $files = [], $use_cache = true)
    {
        $cached_petition_yaml = $this->cache->getItem(self::STORAGE_KEY);
        if ($cached_petition_yaml && $use_cache && $cached_petition_yaml->isHit()) {
            $this->petitions = $cached_petition_yaml->get();
        } else {
            $files = $files ?: $this->defaultFiles;
            $this->logger->info("{class}: loading {file} from filesystem", ['class' => __CLASS__, 'file' => implode(',', $files)]);
            $this->petitions = self::parse($files);

            $cached_petition_yaml->set($this->petitions);
            $cached_petition_yaml->expiresAfter(\DateInterval::createFromDateString('6 hours'));
            $this->cache->save($cached_petition_yaml);
        }

        foreach ($this->petitions as $o) {
            $this->petitionsBySlug[$o->getSlug()] = $o;
        }
        $this->initialized = true;
    }

    private static function parse(array $files): array
    {
        $petitions = [];

        $objectNormalizer = new ObjectNormalizer(
            null,
            new CamelCaseToSnakeCaseNameConverter(),
            null,
            new ReflectionExtractor()
        );

        $serializer = new Serializer(
            [new DateTimeNormalizer(), new ArrayDenormalizer(), $objectNormalizer],
            [new YamlEncoder()]
        );

        foreach ($files as $file) {
            if (! file_exists($file)) {
                continue;
            }

            $objs = $serializer->deserialize(
                file_get_contents($file),
                Petition::class . '[]',
                'yaml',
                [ObjectNormalizer::IGNORED_ATTRIBUTES => ['signatures']]
            );

            foreach ($objs as $o) {
                if (! empty($petitions[$o->getId()])) {
                    throw new \Exception(__CLASS__ . ': Duplicated id: ' . $o->getId());
                }

                $petitions[$o->getId()] = $o;
            }
        }

        $dups = self::arrayDuplicates(
            array_map(
                function ($e) {
                    return $e->getSlug();
                },
                $petitions
            )
        );
        if ($dups) {
            throw new \Exception(__CLASS__ . ': Duplicated slugs: ' . implode(',', $dups));
        }

        return $petitions;
    }

    public static function arrayDuplicates($a)
    {
        return array_values(array_unique(array_diff_key($a, array_unique($a))));
    }

    public function getPetitions($by_slug = false): array
    {
        if (! $this->initialized) {
            $no_cache = $_ENV['NO_CACHE'] ?? getenv('NO_CACHE') ?? false;
            $this->setConfig([], !$no_cache);
        }
        return $by_slug ? $this->petitionsBySlug : $this->petitions;
    }

    public function find($id, $lockMode = null, $lockVersion = null): ?object
    {
        return $this->getPetitions()[$id] ?? null;
    }

    private function findBySlug(string $slug)
    {
        return $this->getPetitions(true)[$slug] ?? null;
    }

    public function findAll(): array
    {
        return $this->getPetitions();
    }

    public function findOneBy(array $criteria, ?array $orderBy = null): ?object
    {
        if (count($criteria) === 1) {
            if (!empty($criteria['slug'])) {
                return $this->findBySlug($criteria['slug']);
            } elseif (!empty($criteria['id'])) {
                return $this->find($criteria['id']);
            }
        }
        throw new \Exception(__CLASS__ . ': Not Implemented');
    }
}
