<?php

namespace App\Repository;

use App\Entity\Signature;
use App\Entity\Petition;
use App\Entity\ObfuscatedSignature;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;

class SignatureRepository extends ServiceEntityRepository
{
    const CACHE_PATTERN = '%kernel.cache_dir%/petition-%d-count.txt';
    protected $params;

    public function __construct(ManagerRegistry $registry, ContainerBagInterface $params)
    {
        $this->params = $params;
        parent::__construct($registry, Signature::class);
    }

    public function getSignatureCount(Petition $petition, string $status = 'accepted')
    {
        $count_override = $this->countOverride($petition, $status);
        if (is_integer($count_override)) {
            return $count_override;
        }

        $qb = $this->createQueryBuilder('p')
            ->select('count(p.id)')
            ->andWhere('p.petition = :petition_id')
            ->andWhere('p.status = :status')
            ->setParameter('petition_id', $petition->getId())
            ->setParameter('status', $status)
            ->getQuery();

        $count = $qb->getSingleScalarResult();
        $obfuscated_count = $this->getArchivedSignatureCount([$petition->getId()]);

        return $count + $obfuscated_count;
    }

    public function getArchivedSignatureCount(array $petition_ids): int
    {
        $petition_ids = array_unique(array_filter(array_map('intval', $petition_ids)));
        if (empty($petition_ids)) {
            return 0;
        }

        $schemaManager = $this->getEntityManager()->getConnection()->getSchemaManager();
        if (! $schemaManager->tablesExist(['obfuscated_signature'])) {
            return 0;
        }

        $qb = $this->getEntityManager()
                   ->getRepository(ObfuscatedSignature::class)
                   ->createQueryBuilder('o')
                   ->select('count(1)')
                   ->andWhere('o.petition IN (:petition_ids)')
                   ->setParameter('petition_ids', $petition_ids)
                   ->getQuery();

        return $qb->getSingleScalarResult();
    }

    public function getArchivedSignaturesCountMulti(array $petition_ids): array
    {
        $petition_ids = array_unique(array_filter(array_map('intval', $petition_ids)));
        if (empty($petition_ids)) {
            return [];
        }

        $schemaManager = $this->getEntityManager()->getConnection()->getSchemaManager();
        if (! $schemaManager->tablesExist(['obfuscated_signature'])) {
            return [];
        }

        $qb = $this->getEntityManager()
                   ->getRepository(ObfuscatedSignature::class)
                   ->createQueryBuilder('o', 'o.petition')
                   ->select('IDENTITY(o.petition) as id', 'count(1) as c', 'p.slug')
                   ->andWhere('o.petition IN (:petition_ids)')
                   ->leftJoin('o.petition', 'p')
                   ->groupBy('o.petition')
                   ->setParameter('petition_ids', $petition_ids)
                   ->getQuery();

        return $qb->getArrayResult();
    }

    /**
     * Allow overriding signature count using a regular file.
     * This allow to modify arbitrarily modify this value.
     * A useful example is merging totals from several distinct petition
     * by using a cron|hook that would create and update regularly such
     * override file containing the desired value.
     * File format is: <accepted-count>[:<bounced-count>[:<revoked-count>[:<invalid-count>]]]
     * Example: 22:3 (22 accepted, 3 bounced, 0 revoked, counted from the database)
     */
    public function countOverride(Petition $petition, string $status) : ?int
    {
        $count_file = $this->getCountOverrideFilename($petition->getId());
        if ($count_file
            && file_exists($count_file)
            && filesize($count_file) >= 1
            && filesize($count_file) <  25
            && ($content = trim(file_get_contents($count_file)))) {
            list($accepted, $bounced, $revoked, $invalid) = array_map('intval', array_pad(explode(':', $content), 4, ''));
            $values = compact("accepted", "bounced", "revoked", "invalid");
            if (isset($values[$status])) {
                return $values[$status];
            }
        }
        return null;
    }

    public function getCountOverrideFilename(int $petition_id) : string
    {
        return sprintf(
            $this->params->resolveString(self::CACHE_PATTERN),
            $petition_id
        );
    }

    public function findExisting(string $email, Petition $petition): ?Signature
    {
        // return $this->findOneBy(['email' => $email, 'petition' => $petition]);
        $qb = $this
            ->createQueryBuilder('s')
            ->where('s.petition = :petition');

        if (preg_match('/@gmail.com$/i', $email)) {
            $qb->andWhere('REPLACE(s.email, :dot, :empty) = :email');
            $qb->setParameter('email', str_replace('.', '', strtolower($email)));
            $qb->setParameter('dot', '.');
            $qb->setParameter('empty', '');
        } else {
            $qb->andWhere('s.email = :email');
            $qb->setParameter('email', strtolower($email));
        }

        return $qb
            ->setParameter('petition', $petition)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
