<?php

namespace App\Repository;

use App\Entity\ObfuscatedSignature;
use App\Entity\Petition;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ObfuscatedSignatureRepository extends ServiceEntityRepository
{

    private $salt;

    public function __construct(ManagerRegistry $registry, string $salt)
    {
        parent::__construct($registry, ObfuscatedSignature::class);
        $this->salt = $salt;
    }

    public function findByPetitionAndEmail(Petition $petition, string $email): int
    {
        $schemaManager = $this->getEntityManager()->getConnection()->getSchemaManager();
        $tableExists = $schemaManager->tablesExist(['obfuscated_signature']);
        if (!$tableExists) {
            return 0;
        }

        return (int)$this->createQueryBuilder('o')
                         ->select('count(o.id)')
                         ->where('o.petition = :petition_id')
                         ->andWhere('o.hashedEmail = :email')
                         ->setParameter('petition_id', $petition->getId())
                         ->setParameter('email', md5($this->salt . $email))
                         ->getQuery()
                         ->getSingleScalarResult();
    }
}
