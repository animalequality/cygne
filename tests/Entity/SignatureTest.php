<?php

namespace App\Tests\Entity;

use App\Entity\Signature;
use App\Entity\Petition;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Validator\Validation;

class SignatureTest extends TestCase
{
    public const EMAILS = [
        // Accept
        ['foo@gmail.con', true],
        ['foo.bar@gmail.com', true],
        ['foo.bar.123@gma.il.com', true],
        ['foo+bar@yahoo.fr', true],
        ['foo+123@yahoo.fr.mobi', true],
        ['foo@yahoo.museum', true],
        // Reject
        ['foo^bar@yahoo.fr', false],
        ['foo/bar@yahoo.fr', false],
        ['foo$bar@yahoo.fr', false],
        ['foo\\bar@yahoo.fr', false],
        ['foo\'bar@yahoo.fr', false],
        ['foo..bar@yahoo.fr', false],
        ['foo.bar.@yahoo.com', false],
    ];

    public function testRegexp()
    {
        $validator = Validation::createValidatorBuilder()
                   ->enableAnnotationMapping(true)
                   ->addDefaultDoctrineAnnotationReader()
                   ->getValidator();

        $sig1 = new Signature();
        $sig1->setPetition(new Petition());
        $sig1->setFirstName('first');
        $sig1->setLastName('last');
        $sig1->setCity('foo');
        $sig1->setPostalCode('123');
        $sig1->setCountry('FR');
        $sig1->setIpAddress('127.0.0.1');
        $sig1->setStatus('accepted');
        $sig1->setNewsletterStatus('todo');
        $sig1->setConfirmEmailStatus(Signature::EMAIL_DELIVERED);
        $sig1->setConfirmEmailId('fake-email-message-id');

        foreach (self::EMAILS as $tuple) {
            [$email, $code] = $tuple;
            $email = trim($email);
            $sig1->setEmail($email);
            $validated = $validator->validate($sig1);

            // (count === 0) === true means validation pass
            $this->assertEquals(count($validated) === 0, $code);
        }
    }
}
