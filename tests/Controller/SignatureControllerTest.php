<?php

namespace App\Tests\Controller;

use App\Controller\SignatureController;
use App\Entity\Petition;
use App\Entity\Signature;
use App\Tests\DatabasePrimer;
use PHPUnit\Framework\TestCase;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class SignatureControllerTest extends WebTestCase
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        $this->client = self::$kernel->getContainer()->get('test.client');
        $this->client->disableReboot();
        DatabasePrimer::prime(self::$kernel);
        $this->_createMock();
    }

    protected function _createMock(): void
    {
        $petition = new Petition();
        $petition->setSlug('foo');
        $petition->setTitle('Foo');
        $petition->setEmailFromName('Name');
        $petition->setEmailFromEmail('email@from_email');
        $petition->setEmailReplyTo('email@from_email');
        $petition->setFormPageUrl('http://blah.com');
        $petition->setShareTwitterUrl('http://twitter.com');
        $petition->setShareFacebookUrl('http://fb.com');

        $this->entityManager->persist($petition);
        $this->entityManager->flush();
    }

    public function testContentType()
    {
        $this->client->request(
            'POST',
            '/api/petitions/1/signatures',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json', 'HTTP_REFERER' => 'http://origin.local'],
            json_encode([
                'email' => 'first@last.com',
                'foo_field' => 'value',
            ])
        );
        $response = $this->client->getResponse();
        $this->assertTrue(
            $response->headers->contains(
                'Content-Type',
                'application/json'
            )
        );
        $this->client->request(
            'POST',
            '/api/petitions/1/signatures',
            [
                'body' => [
                    'email' => 'first@last.com',
                    'foo_field' => 'value'
                ]
            ],
            [],
            ['CONTENT_TYPE' => 'application/x-www-form-urlencoded'],
        );
        $response = $this->client->getResponse();

        $this->assertFalse(
            $response->headers->contains(
                'Content-Type',
                'application/json'
            )
        );
        $this->assertInstanceOf(\Symfony\Component\HttpFoundation\RedirectResponse::class, $response);
        $this->assertStringContainsString('Redirecting', $response->getContent());
        $this->assertArrayHasKey('location', $response->headers->all());
        $this->assertNotEmpty($response->getTargetUrl());
    }

    public function testRedirection()
    {
        $this->client->request(
            'POST',
            '/api/petitions/999/signatures',
            [],
            [],
            ['CONTENT_TYPE' => 'application/x-www-form-urlencoded', 'HTTP_REFERER' => 'http://origin.local'],
        );

        $response = $this->client->getResponse();
        $this->assertEquals($response->getTargetUrl(), 'http://origin.local', 'Non existing petition redirects to referer');

        $this->client->request(
            'POST',
            '/api/petitions/1/signatures',
            [],
            [],
            ['CONTENT_TYPE' => 'application/x-www-form-urlencoded', 'HTTP_REFERER' => 'http://origin.local'],
        );

        $response = $this->client->getResponse();
        $this->assertEquals($response->getTargetUrl(), 'http://blah.com', 'Existing petition redirects to its URL');

        $this->client->request(
            'POST',
            '/api/petitions/1/signatures',
            [],
            [],
            ['CONTENT_TYPE' => 'application/x-www-form-urlencoded', 'HTTP_REFERER' => 'http://origin.local'],
            http_build_query(['redirect_error' => 'http://base.com/?error', 'redirect' => 'http://base.com/ignored'])
        );

        $response = $this->client->getResponse();
        $this->assertEquals($response->getTargetUrl(), 'http://base.com/?error', 'Use "redirect_error" if it exists');

        $this->client->request(
            'POST',
            '/api/petitions/1/signatures',
            [],
            [],
            ['CONTENT_TYPE' => 'application/x-www-form-urlencoded', 'HTTP_REFERER' => 'http://origin.local'],
            http_build_query(['redirect' => 'http://base.com/redirect'])
        );

        $response = $this->client->getResponse();
        $this->assertEquals($response->getTargetUrl(), 'http://base.com/redirect', 'Use "redirect" if it exists');
    }

    public function testRedirectErrorSubstitution()
    {
        $this->client->request(
            'POST',
            '/api/petitions/1/signatures',
            [],
            [],
            ['CONTENT_TYPE' => 'application/x-www-form-urlencoded', 'HTTP_REFERER' => 'http://origin.local'],
            http_build_query(['redirect_error' => 'http://base.com/?errors=%errors%'])
        );

        $response = $this->client->getResponse();

        $this->assertStringContainsString('http://base.com/?errors=', $response->getTargetUrl(), '"redirect_error" is considered');
        $this->assertMatchesRegularExpression('/firstName/', $response->getTargetUrl(), 'Substitutions happen');

        $output = [];
        parse_str(parse_url($response->getTargetUrl(), PHP_URL_QUERY), $output);
        $this->assertArrayHasKey('errors', $output);
        $this->assertMatchesRegularExpression('/This value should not be blank./', $output['errors'], '%errors% is substituted');
    }

    public function testRedirectSubstitution()
    {
        $this->client->request(
            'POST',
            '/api/petitions/1/signatures',
            [],
            [],
            ['CONTENT_TYPE' => 'application/x-www-form-urlencoded', 'HTTP_REFERER' => 'http://origin.local'],
            http_build_query(['redirect' => 'http://base.com/?my_status=%success%&my_errors=%errors%'])
        );

        $response = $this->client->getResponse();

        $this->assertStringContainsString('http://base.com/?', $response->getTargetUrl(), '"redirect" is considered');

        $output = [];
        parse_str(parse_url($response->getTargetUrl(), PHP_URL_QUERY), $output);
        $this->assertArrayHasKey('my_errors', $output);
        $this->assertMatchesRegularExpression('/This value should not be blank./', $output['my_errors'], '%errors% is substituted');
        $this->assertArrayHasKey('my_status', $output);
        $this->assertEquals($output['my_status'], '0', '%success% is substituted');
    }

    public function testCleanup()
    {
        $this->assertEquals(
            SignatureController::cleanEmail('test1.xx.bar+blah.good@EMAIL.com'),
            'test1.xx.bar@email.com'
        );

        $this->assertEquals(
            SignatureController::cleanEmail('test2.xx.bar+blah.good@gmaIL.com'),
            'test2.xx.bar@gmail.com',
        );

        $this->assertEquals(
            SignatureController::cleanEmail('test3.xx.bar+blah.good@gmaIL.com', true),
            'test3xxbar@gmail.com',
        );

        $this->assertEquals(
            SignatureController::cleanEmail(
                SignatureController::cleanEmail('test4.xx.bar+blah.good@gmaIL.com'),
                true
            ),
            'test4xxbar@gmail.com',
        );
    }

    public function testEmailNormalize()
    {
        $this->client->request(
            'POST',
            '/api/petitions/1/signatures',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json', 'HTTP_REFERER' => 'http://origin.local'],
            json_encode([
                'email' => '  first@LAST.com  ',
                'firstName' => 'first',
                'lastName' => 'last',
                'city' => 'city',
                'postalCode' => '012345'
            ])
        );

        $response = $this->client->getResponse();
        $test1 = $this->entityManager
               ->getRepository(Signature::class)
               ->findOneBy([]);

        $this->assertEquals(
            $test1->getEmail(),
            'first@last.com',
            'Email is trim() and lowercased'
        );
    }
}
