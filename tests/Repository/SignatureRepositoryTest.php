<?php

namespace App\Tests\Repository;

use App\Entity\Signature;
use App\Entity\Petition;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use App\Tests\DatabasePrimer;

class SignatureRepositoryTest extends KernelTestCase
{

    /**
     * @var \Doctrine\ORM\EntityManager
     */
    private $entityManager;

    /**
     * {@inheritDoc}
     */
    protected function setUp(): void
    {
        $kernel = self::bootKernel();

        $this->entityManager = $kernel->getContainer()->get('doctrine.orm.entity_manager');
        DatabasePrimer::prime(self::$kernel);
    }

    public function testfindExisting()
    {
        $petition = new Petition();
        $petition->setSlug('foo');
        $petition->setTitle('Foo');
        $petition->setEmailFromName('Name');
        $petition->setEmailFromEmail('email@from_email');
        $petition->setEmailReplyTo('email@from_email');
        $petition->setFormPageUrl('http://blah.com');
        $petition->setShareTwitterUrl('http://twitter.com');
        $petition->setShareFacebookUrl('http://fb.com');

        $sig1 = new Signature();
        $sig1->setPetition($petition);
        $sig1->setFirstName('first');
        $sig1->setLastName('last');
        $sig1->setEmail('first.dot@last.com');
        $sig1->setIpAddress('127.0.0.1');
        $sig1->setStatus('accepted');
        $sig1->setNewsletterStatus('todo');
        $sig1->setConfirmEmailStatus(Signature::EMAIL_DELIVERED);
        $sig1->setConfirmEmailId('fake-email-message-id');

        $sig2 = clone($sig1);
        $sig2->setEmail('me.bar@gmail.com');

        $this->entityManager->persist($petition);
        $this->entityManager->persist($sig1);
        $this->entityManager->persist($sig2);
        $this->entityManager->flush();

        $test1 = $this->entityManager
                    ->getRepository(Signature::class)
                    ->findExisting('first.dot@last.com', $petition);
        $this->assertNotNull($test1);

        $test2 = $this->entityManager
                    ->getRepository(Signature::class)
                    ->findExisting('fIrStdot@last.com', $petition);
        $this->assertNull($test2);

        $test3 = $this->entityManager
                    ->getRepository(Signature::class)
                    ->findExisting('m.eb.A.R@gmail.com', $petition);
        $this->assertNotNull($test3);
    }

    /**
     * {@inheritDoc}
     */
    protected function tearDown(): void
    {
        parent::tearDown();

        $this->entityManager->close();
        $this->entityManager = null; // avoid memory leaks
    }
}
