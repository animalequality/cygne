# Cygne – Petition Platform

## Dependencies

* PHP 7.2+
* Symfony 4+
* MySQL
* Mandrill

## Setup

### Run database migrations

```shell
php bin/console doctrine:migrations:migrate
```

### Run development server

```shell
php bin/console server:run
```

### Show available routes

```shell
php bin/console debug:router
```

## Commands

### Create a new petition

```shell
php bin/console petition:create
```

### List petitions

```shell
php bin/console petition:list
```

### Get all infos about a petition

```shell
php bin/console petition:info [petiionId]
```

## Crontabs

### Send confirmation emails

```shell
php bin/console signatures:send-email
```

## Deployment (General)

See: [How to Deploy a Symfony Application](https://symfony.com/doc/current/deployment.html)

### Install production dependencies

```shell
composer install --no-dev --optimize-autoloader
```

### Clear & warmup cache

```shell
php bin/console cache:clear --env=prod --no-debug --no-warmup
php bin/console cache:warmup --env=prod
```

## Deployment on Platform.sh

### Set environment variables

Non-sensitive variables are setup in the `.platform.app.yaml` configuration file, as described in [Configuration / Variables (Platform.sh)](https://docs.platform.sh/configuration/app/variables.html).

Sensitive variables (access tokens, password, etc.) are not stored in `.platform.app.yaml` to avoid adding sensitive infos to the Git repository. The approach that we took below is to set the variables at the environment level (on the `master` environment) and enable inheritence (`--inheritable=true`) so that child environments (e.g. `dev`) will get these values as well:

```shell
platform variable:create --no-wait            \
                         --level=environment  \
                         --environment=master \
                         --sensitive=true     \
                         --json=false         \
                         --enabled=true       \
                         --inheritable=true   \
                         --prefix=env         \
                         --name CRON_PASS
```

Here are the required variables:

* `APP_SECRET`
* `CRON_PASS`
* `CHECK_PASS`
* `MANDRILL_KEY`
* `MANDRILL_WEBHOOK_KEY`
* `RECAPTCHA_SECRET`

It's possible to list the currently setup variables:

```shell
platform variable:list --environment=master
```

Read more about [Variables on Platform.sh](https://docs.platform.sh/development/variables.html).


### Migrate data to Platform.sh

Import SQL dump:

```shell
platform sql --environment=master < dump.sql
```

## Models

### Petition

MySQL Table: `petition`

Fields:

* `id`: *integer (autoincrement)*, unique petition identifier
* `createdAt`: *datetime*, time when the petition was created
* `slug`: *string (unique, indexed)*, petition slug, used to uniquely identify the petition in a human readable way
* `title`: *string*, a display title
* `requirements`: *unsigned smallint*, Represents which signature fields this petition requires (default to all)

### Signature

MySQL Table: `signature`

Fields:
* `id`: *integer*, unique signature identifier
* `createdAt`: *datetime*, time when the signature was created
* `revokedAt`: *datetime*, time when the signature was revoked/email has bounced (if applicable)
* `firstName`: *string*, signer's first name
* `lastName`: *string*, signer's last name
* `email`: *string*, signer's email address
* `city`: *string*, signer's city
* `postalCode`: *string*, signer's postal code
* `country`: *string*, signer's country (ISO code)
* `ipAddress`: *string*, signer's IP address (IPv4 or IPv6)
* `status`: *string*, status of the signature
     * `accepted` (default), the signature has been confirmed by our server
     * `revoked`: the user has asked us to revoke their signature
     * `bounced`: the confirmation email has bounced
     * `invalid`: invalidated as spam/bot/fake/...
* `confirmEmailStatus`: *string*, status of the confirmation email
     * `todo` (default): email has not yet been sent
     * `sent`: email has been sent (used to be "done")
     * `delivered`: email has been received (SNS confirmation)
     * `error`: failed to send email
     * `ignore`: for some unknown reason, we chose to ignore this email
* `confirmEmailId`: *string*, MessageId of the confirmation email
* `confirmEmailError`: *string*, confirmation email error
* `newsletterStatus`: *string*, status of the newsletter registration
     * `todo` (default): signer has approved the subscription to the newsletter, but it hasn't yet been processed
     * `done`: subscription has been successfully completed
     * `error`: subscription has failed
     * `ignore`: signer has rejected to receive the newsletter or we just chose to ignore this subscription
* `newsletterError`: *string*, newsletter registration email error
* `extra`: *string*, opaque JSON-encoded 4k payload processed by synchronizer

## API Endpoints

### Get petition info

***Request:***

```http
GET /api/petitions/{petitionId}
```

***Response:***

```http
200 OK
```

```json
{
  "id": 1,
  "signatureCount": "75574",
  "slug": "emmanuel-macron-stop-aux-cages-dans-tous-les-elevages-de-poules-pondeuses",
  "title": "Stop à l’élevage des poules en cage !"
}
```

### Sign a petition

***Request:***

```http
POST /api/petitions/{petitionId}/signatures
```

```json
{
  "firstName": "Super",
  "lastName": "Vegan",
  "email": "super.vegan@example.com",
  "city": "Loveville",
  "postalCode": "75001",
  "country": "fr",
  "newsletter": true,
  "recaptcha": "RECAPTCHA_TOKEN"
}
```

***Response:***

```http
201 CREATED
```

```json
{
  "status": "accepted",
  "firstName": "Super",
  "lastName": "Vegan",
  "email": "super.vegan@example.com",
  "city": "Loveville",
  "postalCode": "75001",
  "country": "fr",
  "newsletter": true
}
```

## Web Endpoints

### View a petition

We may display here a page with the petition title + count.

```http
GET /petitions/:petitionId
```

### Revoke a signature

This page will ask users if they want to actually revoke their signature.

```http
GET /petitions/:petitionId/revoke/:signatureId?token=SECURITY_TOKEN
```

A `POST` request on this endpoint will actually revoke the signature.

```http
POST /petitions/:petitionId/revoke/:signatureId?token=SECURITY_TOKEN
```

### YAML petition storage

There is a way to provision database petition from Yaml relying on Symfony built-in entity (de)serialization process.
1. Dump existing db-stored petitions to stdout: `./bin/console --no-debug -v petition:dump` and notice how `signatures` are ignored and date exported to RFC-3339.
2. Override database tuples using YAML to update/insert (but currently not delete) petitions using `./bin/console --no-debug -v petition:sync-from-yaml config/petitions.yaml`
3. Assuming `vars/main.yml` is created and petitions stored under the `petitions` key,  petition can then be managed via Ansible:
```yaml
# tasks/main.yml
- name: Copy petitions YAML data
  copy:
    dest: "{{ your-directory }}/config/petitions.yaml"
    content: |
      #jinja2: lstrip_blocks: True
      {{ petitions | to_nice_yaml(indent=4) }}
  notify: cygne-reload-petitions

# handlers/main.yml
- name: cygne-reload-petitions
  command: chdir="{{ your-directory }}" bin/console --no-debug -vf petition:sync-from-yaml
  become: yes
```

## SQL Cheatsheet

### List signatures by French department

```sql
SELECT LEFT(`postal_code`, 2) as `dept`, COUNT(*) FROM `signature` WHERE `petition_id` = 1 AND `country` = 'FR' AND `postal_code` REGEXP '^([0-9][1-9]|[1-9][0-9])' GROUP BY `dept` ORDER BY `dept`;
```

## Export Signatures example

```
platform sql -e master --raw "SELECT first_name,last_name,postal_code,city,country,created_at FROM signature WHERE petition_id = 9 AND status = 'accepted';" | tr '\t' ',' > ~/Downloads/avril-signatures.csv
```
